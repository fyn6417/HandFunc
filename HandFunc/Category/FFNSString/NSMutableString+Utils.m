//
//  NSMutableString+Utils.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/5.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "NSMutableString+Utils.h"

@implementation NSMutableString (Utils)

+ (NSAttributedString *)ff_attributedStringWithString:(NSString *)string color:(UIColor *)color font:(UIFont *)font
{
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSForegroundColorAttributeName] = color;
    attrs[NSFontAttributeName] = font;
    NSAttributedString *attributedStr = [[NSAttributedString alloc] initWithString:string attributes:attrs];
    return attributedStr;
}


+ (NSAttributedString *)ff_attributedStringWithString:(NSString *)string space:(CGFloat)space font:(UIFont *)font
{
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
//    paraStyle.lineSpacing = space; //设置行间距
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    NSDictionary *dic = @{NSFontAttributeName:font, NSKernAttributeName:[NSNumber numberWithFloat:space]};
    NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:string attributes:dic];
    return attributeStr;
}

+ (NSAttributedString *)ff_attributedStringWithString:(NSString *)string space:(CGFloat)space
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string attributes:@{NSKernAttributeName:@(space)}];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [string length])];

    return attributedString;
}



@end
