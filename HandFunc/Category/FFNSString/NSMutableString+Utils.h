//
//  NSMutableString+Utils.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/5.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableString (Utils)

+ (NSAttributedString *)ff_attributedStringWithString:(NSString *)string color:(UIColor *)color font:(UIFont *)font;

+ (NSAttributedString *)ff_attributedStringWithString:(NSString *)string space:(CGFloat)space font:(UIFont *)font;

+ (NSAttributedString *)ff_attributedStringWithString:(NSString *)string space:(CGFloat)space;

@end
