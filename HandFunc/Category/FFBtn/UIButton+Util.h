//
//  UIButton+Util.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/17.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Util)

- (UIButton *)verticalImage:(UIImage *)img
                      Title:(NSString *)title
                      space:(CGFloat)space
                  titleFont:(UIFont *)font
                    textAli:(NSTextAlignment)ali
              normalBgColor:(UIColor *)normalBgColor
           higtlightBgColor:(UIColor *)highlightBgColor
                     target:(id)target
                        sel:(SEL)sel
                      frame:(CGRect)frame;

@end
