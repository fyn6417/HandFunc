//
//  UIButton+Util.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/17.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "UIButton+Util.h"

@implementation UIButton (Util)

- (UIButton *)verticalImage:(UIImage *)img
                      Title:(NSString *)title
                      space:(CGFloat)space
                  titleFont:(UIFont *)font
                    textAli:(NSTextAlignment)ali
              normalBgColor:(UIColor *)normalBgColor
           higtlightBgColor:(UIColor *)highlightBgColor
                     target:(id)target
                        sel:(SEL)sel
                      frame:(CGRect)frame
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame; // CGRectMake(100, 100,90, 90);
    button.backgroundColor = [UIColor whiteColor];//button的背景颜色
    
    [button setImage:img forState:UIControlStateNormal];//给button添加image
//    button.imageEdgeInsets = UIEdgeInsetsMake(5,13,21,button.titleLabel.bounds.size.width);//设置image在button上的位置（上top，左left，下bottom，右right）这里可以写负值，对上写－5，那么image就象上移动5个像素
    button.imageEdgeInsets = UIEdgeInsetsMake(-frame.size.height/4, frame.size.width/2-img.size.width/2, 0, 0);
    
    [button setTitle:title forState:UIControlStateNormal];//设置button的title
    button.titleLabel.font = font;
    button.titleLabel.textAlignment = ali;//设置title的字体居中
    [button setTitleColor:normalBgColor forState:UIControlStateNormal];//设置title在一般情况下为白色字体
    [button setTitleColor:highlightBgColor forState:UIControlStateHighlighted];//设置title在button被选中情况下为灰色字体
    button.titleEdgeInsets = UIEdgeInsetsMake(frame.size.height/2, -20 , 0, 0);
    [button addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    
    return button;   
}

@end
