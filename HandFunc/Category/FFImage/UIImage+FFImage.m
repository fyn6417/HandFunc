//
//  UIImage+FFImage.m
//  HandFunc
//
//  Created by ff'Mac on 2017/6/28.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "UIImage+FFImage.h"

@implementation UIImage (FFImage)

+ (UIImage *)ff_imgWithOriginalImg:(NSString *)ff_imgName
{
    UIImage *image = [UIImage imageNamed:ff_imgName];
    return [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}

@end
