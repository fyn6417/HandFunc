//
//  UIImage+FFImage.h
//  HandFunc
//
//  Created by ff'Mac on 2017/6/28.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (FFImage)

+ (UIImage *)ff_imgWithOriginalImg:(NSString *)ff_imgName;

@end
