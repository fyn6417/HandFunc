//
//  FFNetwork.m
//  ZDTalk
//
//  Created by 0xday on 2017/4/14.
//  Copyright © 2017年 Zaodao. All rights reserved.
//

#import "FFNetwork.h"

@implementation FFNetwork

+ (instancetype)sharedManager
{
    static FFNetwork *manager = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        manager = [[FFNetwork alloc] initWithBaseURL:[NSURL URLWithString:@""]];
    });
    return manager;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    if ([[url path] length] > 0 && ![[url absoluteString] hasSuffix:@"/"]) {
        url = [url URLByAppendingPathComponent:@""];
    }
    
    if (self = [super initWithBaseURL:url]) {
        /**设置请求超时时间*/
        self.requestSerializer.timeoutInterval = 3;
        /**设置相应的缓存策略*/
        self.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        /**分别设置请求以及相应的序列化器*/
        self.requestSerializer = [AFHTTPRequestSerializer serializer];

        AFJSONResponseSerializer * response = [AFJSONResponseSerializer serializer];
        response.removesKeysWithNullValues = YES;
        self.responseSerializer = response;
        
        /**复杂的参数类型 需要使用json传值-设置请求内容的类型*/
        //[self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        /**设置接受的类型*/
        [self.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"text/plain",@"application/json",@"text/json",@"text/javascript",@"text/html", nil]];
        self.securityPolicy.allowInvalidCertificates = YES;
        self.securityPolicy.validatesDomainName=NO;
    }
    return self;
}

- (void)ff_requsetWithMethod:(HTTPMethod)method
                     path:(NSString *)path
                   params:(id)params
                  success:(requsetSuccessBlock)success
                  failure:(requsetFailureBlock)failure
{
    if (method == POST) {
        [self POST:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            success(responseObject);
            
//            NSLog(@"-------->Response%@", responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            failure(error);
        }];
    }
    else
    {
        //GET
        
        [self GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            failure(error);
        }];
    }
}



+ (void)downloadWithRequest:(NSURLRequest *)request targetPath:(NSString *)path shouldResume:(BOOL)shouldResume
{
    NSURLSession * session = [NSURLSession sharedSession];
    NSURLSessionDownloadTask * downloadTask = [session downloadTaskWithRequest:request completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        //location 下载到沙盒的地址
        NSLog(@"下载完成%@",location);
        
        //response.suggestedFilename 响应信息中的资源文件名
        NSString * cachesPath =  [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:response.suggestedFilename];
        
        NSLog(@"缓存地址%@",cachesPath);
        
        //获取文件管理器
        NSFileManager * mgr = [NSFileManager defaultManager];
        //将临时文件移动到缓存目录下
        //[NSURL fileURLWithPath:cachesPath] 将本地路径转化为URL类型
        //URL如果地址不正确，生成的url对象为空
        
        [mgr moveItemAtURL:location toURL:[NSURL fileURLWithPath:path] error:NULL];
        
    }];
    
    [downloadTask resume];
}

+ (void)downloadWithURL:(NSString *)urlString targetPath:(NSString *)path
{
    NSURL * url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@", urlString] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    NSURLSessionDownloadTask * downloadTask = [session downloadTaskWithURL:url completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        //location 下载到沙盒的地址
        NSLog(@"下载完成%@",location);
        
        //response.suggestedFilename 响应信息中的资源文件名
        NSString * cachesPath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:response.suggestedFilename];
        
        NSLog(@"缓存地址%@",cachesPath);
        
        //获取文件管理器
        NSFileManager * mgr = [NSFileManager defaultManager];
        //将临时文件移动到缓存目录下
        //[NSURL fileURLWithPath:cachesPath] 将本地路径转化为URL类型
        //URL如果地址不正确，生成的url对象为空
        
        [mgr moveItemAtURL:location toURL:[NSURL fileURLWithPath:path] error:NULL];
        
    }];
    
    [downloadTask resume];
}

+ (void)ff_downloadTask:(NSURLSessionDownloadTask *)task request:(NSURLRequest *)request targetPath:(NSString *)path
{
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    task = [session downloadTaskWithRequest:request completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSFileManager * mgr = [NSFileManager defaultManager];
        
        //URL如果地址不正确，生成的url对象为空
        BOOL isDirectory;
        if(![[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDirectory]) {
            isDirectory = NO;
        }
        // if targetPath is a directory, use the file name we got from the urlRequest.
        if (isDirectory) {
            NSString *fileName = [request.URL lastPathComponent];
            NSString *targetPath = [NSString pathWithComponents:[NSArray arrayWithObjects:path, fileName, nil]];
            [mgr moveItemAtURL:location toURL:[NSURL fileURLWithPath:targetPath] error:NULL];
        }else {
            [mgr moveItemAtURL:location toURL:[NSURL fileURLWithPath:path] error:NULL];
        }
    }];
    [task resume];
}

+ (void)ff_sessionType:(NSURLSession *)session taskType:(NSURLSessionTask *)task request:(NSURLRequest *)request targetPath:(NSString *)path
{
    task = [session downloadTaskWithRequest:request completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSFileManager * mgr = [NSFileManager defaultManager];
        
        //URL如果地址不正确，生成的url对象为空
        BOOL isDirectory;
        if(![[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDirectory]) {
            isDirectory = NO;
        }
        // if targetPath is a directory, use the file name we got from the urlRequest.
        if (isDirectory) {
            NSString *fileName = [request.URL lastPathComponent];
            NSString *targetPath = [NSString pathWithComponents:[NSArray arrayWithObjects:path, fileName, nil]];
            [mgr moveItemAtURL:location toURL:[NSURL fileURLWithPath:targetPath] error:NULL];
        }else {
            [mgr moveItemAtURL:location toURL:[NSURL fileURLWithPath:path] error:NULL];
        }
    }];
    [task resume];
}



+ (NSURLSession *)backgroundSession
{
    static NSURLSession *backgroundSession = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"com.shinobicontrols.BackgroundDownload.BackgroundSession"];
        backgroundSession = [NSURLSession sessionWithConfiguration:config];
        
    });
    
    return backgroundSession;
}

+ (NSURLSession *)defaultSession
{
    static NSURLSession *defaultSession = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        defaultSession = [NSURLSession sessionWithConfiguration:config];
        
    });
    
    return defaultSession;
}

+ (void)ff_downloadTaskWithRequest:(NSURLRequest *)request targetPath:(NSString *)path
{
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionDownloadTask *task = [session downloadTaskWithRequest:request completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSFileManager * mgr = [NSFileManager defaultManager];
        
        //URL如果地址不正确，生成的url对象为空
        BOOL isDirectory;
        if(![[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDirectory]) {
            isDirectory = NO;
        }
        // if targetPath is a directory, use the file name we got from the urlRequest.
        if (isDirectory) {
            NSString *fileName = [request.URL lastPathComponent];
            NSString *targetPath = [NSString pathWithComponents:[NSArray arrayWithObjects:path, fileName, nil]];
            [mgr moveItemAtURL:location toURL:[NSURL fileURLWithPath:targetPath] error:NULL];
        }else {
            [mgr moveItemAtURL:location toURL:[NSURL fileURLWithPath:path] error:NULL];
        }
    }];
    [task resume];
}

@end


