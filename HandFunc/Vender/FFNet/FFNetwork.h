//
//  FFNetwork.h
//  ZDTalk
//
//  Created by 0xday on 2017/4/14.
//  Copyright © 2017年 Zaodao. All rights reserved.
//

//#import <AFNetworking/AFNetworking.h>
#import <AFNetworking.h>
#import "AFHTTPSessionManager.h"

typedef void (^requsetSuccessBlock)(NSDictionary *dic);
typedef void (^requsetFailureBlock)(NSError *err);

typedef NS_ENUM(NSUInteger, HTTPMethod) {
    GET,
    POST
};

@interface FFNetwork : AFHTTPSessionManager

@property (assign, readonly) BOOL shouldResume;

+ (instancetype)sharedManager;

+ (NSURLSession *)backgroundSession;

+ (NSURLSession *)defaultSession;

- (void)ff_requsetWithMethod:(HTTPMethod)method
                        path:(NSString *)path
                      params:(id)params
                     success:(requsetSuccessBlock)success
                     failure:(requsetFailureBlock)failure;

+ (void)ff_downloadTask:(NSURLSessionDownloadTask *)task request:(NSURLRequest *)request targetPath:(NSString *)path;
+ (void)ff_sessionType:(NSURLSession *)session taskType:(NSURLSessionTask *)task request:(NSURLRequest *)request targetPath:(NSString *)path;
@end

