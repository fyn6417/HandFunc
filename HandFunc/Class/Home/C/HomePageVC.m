//
//  HomePageVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/6/28.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "HomePageVC.h"

@interface HomePageVC ()<FFTitleBarDelegate, HomeHeaderViewDelegate, UITableViewDelegate, UITableViewDataSource>
{
    FFTitleBar *v_titleBar;
    
    HomeHeaderView *headerView;
    UITableView *table_content;
    NSArray *arr_content;
}
@end

@implementation HomePageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
//    [self ff_checkLoginState];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFWhiteColor;
    self.navigationController.navigationBar.hidden = YES;
    
    arr_content = @[@{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"icon_paper_2"},
                    @{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"icon_paper_1"},
                    @{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"icon_paper_2"},
                    @{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"icon_paper_1"},
                    @{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"icon_paper_2"},
                    @{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"icon_paper_1"}];
}

#pragma mark - Build View Method

- (void)ff_buildView
{
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"首页" leftImg:FFImg(@"icon_back") rightImg:FFImg(@"Icon20")];
    v_titleBar.frame = CGRectMake(0, 0, SCREENW, 64*ss);
    v_titleBar.delegate = self;
    v_titleBar.hidden = YES;
    
    [self.view addSubview:v_titleBar];
    
    table_content = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    //    table_content.frame = CGRectMake(0, 74*ss, SCREENW, SCREENH-49*ss-74*ss);
    table_content.delegate = self;
    table_content.dataSource = self;
    table_content.backgroundColor = FFBaseBGColor;
    HomeHeaderView *view = [[HomeHeaderView alloc] init];
    view.delegate = self;
    table_content.tableHeaderView = view;
    table_content.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:table_content];
}
#pragma mark - Layout View Method

- (void)ff_layoutView
{
    [table_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREENW, SCREENH-49*ss));
    }];
}

#pragma mark - HomeHeaderViewDelegate Method

- (void)ff_btnFliterAction
{
    [self ff_gotoFliterVC];
}

- (void)ff_btnSearchAction
{
    [self ff_gotoSearchVC];
}

- (void)ff_tapWithIndex:(NSInteger)index
{
    if (index == 0) {
        [self ff_gotoPlatformVC];
    } else if (index == 1) {
        [self ff_gotoMsgVC];
    } else {
        [self ff_gotoNewPostVC];
    }
}

#pragma mark - Goto Method

- (void)ff_gotoFliterVC
{
    HomeFliterVC *vc = [HomeFliterVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)ff_gotoSearchVC
{
    HomeSearchVC *vc = [HomeSearchVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)ff_gotoPlatformVC
{
//    HomePlatformVC *vc = [HomePlatformVC new];
//    [self.navigationController pushViewController:vc animated:YES];
    HomeFliterVC *vc = [HomeFliterVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)ff_gotoMsgVC
{
    HomeMsgVC *vc = [HomeMsgVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)ff_gotoNewPostVC
{
    HomeNewPostVC *vc = [HomeNewPostVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITableViewDelegate Method

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    return [[HomeHeaderView alloc] init];
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    return arr_content.count;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return [arr_content[section] count];
    return arr_content.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10*ss;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cID = @"cid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cID];
    }
    
//    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;

    HomePageCellModel *model = [HomePageCellModel yy_modelWithDictionary:arr_content[row]];
    HomePageCellView *view = [[HomePageCellView alloc] initWithModel:model];
    view.frame = CGRectMake(0, 0, SCREENW-18*2*ss, 200);
    [cell.contentView addSubview:view];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        NSLog(@"你好好啊hi安师大会谁都爱花生豆暗红色的");
    }
    HomePagePaperDetailVC *vc = [HomePagePaperDetailVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Check Login Method

- (void)ff_checkLoginState
{
    if (![FFUtils ff_isLogin]) {
        //未登录
        [self presentViewController:[SignUpVC new] animated:YES completion:nil];
    }
}

@end
