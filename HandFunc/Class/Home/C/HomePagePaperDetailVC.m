//
//  HomePagePaperDetailVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/8/22.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "HomePagePaperDetailVC.h"

@interface HomePagePaperDetailVC ()<FFTitleBarDelegate>
{
    FFTitleBar *v_titleBar;
    UITextView *tv_content;
    NSString *str_content;
}
@end

@implementation HomePagePaperDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    //    [self ff_checkLoginState];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFWhiteColor;
    self.navigationController.navigationBar.hidden = YES;
    
    str_content = @"手的功能恢复\
    \n对待手部损伤，除及时正确地进行初期外科处理和必要的晚期处理外，还应该充分发挥伤员的主观能动作用，积极地、长期地进行手的功能锻炼，多使用患手，应用理疗、体疗、弹性夹板等辅助方法，争取最大限度地恢复手的功能。\
    \n一、功能锻炼\
    \n分主动及被动锻炼，应从早期开始，有计划地进行。在石膏固定期间以主动锻炼为主，积极活动未固定的手指及上肢的各关节。固定部位亦可作肌肉静力收缩练习（肌腱缝合术后早期不作）。去除固定后，仍以主动活动为主，亦需逐渐作关节被动活动。要求伤员在医生指导下长期刻苦锻炼，从轻到重、从小到大地活动每个关节。此外，积极使用患手是最好的功能锻炼方式，日常生活及工作中应尽量运用患手，如拿筷子、执笔、扣钮扣和系鞋带以及使用钳子、螺丝刀等工具、也可执钢球、玻璃球练习。总之，要充分发挥伤员的积极性，主动坚持功能锻炼。\
    \n二、物理疗法及体育疗法\
    \n理疗能促进手部循环，消除水肿和软化疤痕等，有利于手的活动，但不能代替功能锻炼。体疗是在医生指导下，对患手作适当的按摩活动，利用各种器械练习关节活动。理疗与体疗相互配合，收效更好。戴上夹板后，利用橡皮筋的弹性拉力。使掌指关节强力被动屈曲，也可做主动的屈伸活动。\
    \n三、支具治疗\
    \n关节活动受限或僵硬的患手，配戴各种弹簧夹板，利用弹簧或橡皮筋的弹性持续牵引，帮助关节主动、被动活动，预防或纠正关节、肌腱、肌肉的粘连与挛缩。\
    \
    \
    \
    \
    \n\
    \n\
    \n\
    \n\
    \n手掌功能恢复办法\
    \n\
    \n问题分析：血管瘤实际是一种先天性血管发育畸形，介于错构瘤和真性肿瘤两者之间，毛细血管瘤是其中类型之一。起因尚不明确，据研究表明大概与家族史有关，有一定的遗传因素。血管瘤患者中家族史患者占10%.血管瘤组织中可测出较高水平的雌激素受体，说明雌激素在血管瘤发病过程中起一定作用，所以血管瘤患者中女孩患病率高于男孩。\
    \n意见建议：血管瘤在临床上分有很多种类型，有的可以自行消退，有的需要及早治疗，建议您先明确所患的血管瘤类型然后在采取针对性的治疗方案.\
";
    /*
     手的功能恢复
     对待手部损伤，除及时正确地进行初期外科处理和必要的晚期处理外，还应该充分发挥伤员的主观能动作用，积极地、长期地进行手的功能锻炼，多使用患手，应用理疗、体疗、弹性夹板等辅助方法，争取最大限度地恢复手的功能。
     一、功能锻炼
     分主动及被动锻炼，应从早期开始，有计划地进行。在石膏固定期间以主动锻炼为主，积极活动未固定的手指及上肢的各关节。固定部位亦可作肌肉静力收缩练习（肌腱缝合术后早期不作）。去除固定后，仍以主动活动为主，亦需逐渐作关节被动活动。要求伤员在医生指导下长期刻苦锻炼，从轻到重、从小到大地活动每个关节。此外，积极使用患手是最好的功能锻炼方式，日常生活及工作中应尽量运用患手，如拿筷子、执笔、扣钮扣和系鞋带以及使用钳子、螺丝刀等工具、也可执钢球、玻璃球练习。总之，要充分发挥伤员的积极性，主动坚持功能锻炼。
     二、物理疗法及体育疗法
     理疗能促进手部循环，消除水肿和软化疤痕等，有利于手的活动，但不能代替功能锻炼。体疗是在医生指导下，对患手作适当的按摩活动，利用各种器械练习关节活动。理疗与体疗相互配合，收效更好。戴上夹板后，利用橡皮筋的弹性拉力。使掌指关节强力被动屈曲，也可做主动的屈伸活动。
     三、支具治疗
     关节活动受限或僵硬的患手，配戴各种弹簧夹板，利用弹簧或橡皮筋的弹性持续牵引，帮助关节主动、被动活动，预防或纠正关节、肌腱、肌肉的粘连与挛缩。
     
     
     
     
     
     
     
     
     手掌功能恢复办法
     
     问题分析：血管瘤实际是一种先天性血管发育畸形，介于错构瘤和真性肿瘤两者之间，毛细血管瘤是其中类型之一。起因尚不明确，据研究表明大概与家族史有关，有一定的遗传因素。血管瘤患者中家族史患者占10%.血管瘤组织中可测出较高水平的雌激素受体，说明雌激素在血管瘤发病过程中起一定作用，所以血管瘤患者中女孩患病率高于男孩。
     意见建议：血管瘤在临床上分有很多种类型，有的可以自行消退，有的需要及早治疗，建议您先明确所患的血管瘤类型然后在采取针对性的治疗方案.

     */
}

#pragma mark - Build View Method

- (void)ff_buildView
{
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"文章详情" leftImg:FFImg(@"icon_back") rightImg:FFImg(@"Icon20")];
    v_titleBar.frame = CGRectMake(0, 0, SCREENW, 64*ss);
    v_titleBar.delegate = self;
//    v_titleBar.hidden = YES;
    
    [self.view addSubview:v_titleBar];
    
    tv_content = [[UITextView alloc] initWithFrame:CGRectZero];
    tv_content.text = str_content;
    tv_content.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:tv_content];
}
#pragma mark - Layout View Method

- (void)ff_layoutView
{
    [tv_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(v_titleBar.mas_bottom).offset(6);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREENW, SCREENH-49*ss));
    }];
}

#pragma mark - FFTitleBarDelegate Method

- (void)ff_leftBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
