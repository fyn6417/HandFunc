//
//  HomePageCellView.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/18.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomePageCellModel.h"

@interface HomePageCellView : UIView

- (instancetype)initWithModel:(HomePageCellModel *)model;

@end
