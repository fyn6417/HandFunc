//
//  HomePageCellView.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/18.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "HomePageCellView.h"

@interface HomePageCellView()
{
    FFLab *lab_title;
    FFLab *lab_description;
    UIImageView *imgv_content;
}

@end

@implementation HomePageCellView

- (instancetype)initWithModel:(HomePageCellModel *)model
{
    self = [super init];
    if (self) {
        lab_title = [[FFLab alloc] initWithText:model.ff_title txtColor:FFColorFromHex(0x414141) txtAli:NSTextAlignmentLeft font:FFFontSize(15*ss)];
        [self addSubview:lab_title];
        
        lab_description = [[FFLab alloc] initWithText:model.ff_description txtColor:FFColorFromHex(0xaaaaaa) txtAli:NSTextAlignmentLeft font:FFFontSize(12*ss)];
        [self addSubview:lab_description];
        
        imgv_content = [[UIImageView alloc] initWithFrame:CGRectZero];
        imgv_content.image = FFImg(model.ff_imgName);
        [self addSubview:imgv_content];
        
        [lab_title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(15*ss);
            make.left.mas_equalTo(self.mas_left).offset(18*ss);
        }];
        
        [lab_description mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(lab_title.mas_bottom).offset(15*ss);
            make.left.mas_equalTo(lab_title.mas_left);
            make.width.mas_equalTo(SCREENW-18*2*ss);
        }];
        
        [imgv_content mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(lab_description.mas_bottom).offset(15*ss);
            make.left.mas_equalTo(lab_title.mas_left);
            make.size.mas_equalTo(CGSizeMake(SCREENW-18*2*ss, 81*ss));
        }];
        
    }
    return self;
}

@end
