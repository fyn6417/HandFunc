//
//  HomeHeaderView.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/16.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "HomeHeaderView.h"

@interface HomeHeaderView()<FFBtnArrDelegate>
{
    UIImageView *imgv_bg;
    
    UIImageView *imgv_logo;
    FFLab *lab_title;
    FFLab *lab_description;
    UIView *v_lineL;
    UIView *v_lineR;
    
    FFBtnArr *v_btnArr;
    FFBtn *btn_fliter;
    FFBtn *btn_search;
    
    FFLab *lab_notice;
}

@end

@implementation HomeHeaderView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = FFGrayColor;
        self.frame = CGRectMake(0, 0, SCREENW, 340*ss);
        
        imgv_bg = [[UIImageView alloc] initWithFrame:CGRectZero];
        imgv_bg.image = FFImg(@"icon_bg_home");
        [self addSubview:imgv_bg];
        
        imgv_logo = [[UIImageView alloc] initWithFrame:CGRectZero];
        imgv_logo.image = FFImg(@"Icon40");
        [self addSubview:imgv_logo];
        
        NSString *str_title = [NSString stringWithFormat:@"%@", [NSMutableString ff_attributedStringWithString:@"中国手功能康复" space:5.0]];
        lab_title = [[FFLab alloc] initWithText:@"中国手功能康复" txtColor:FFColorFromHex(0xf9f9f9) txtAli:NSTextAlignmentCenter font:FFBoldFontSize(18*ss)];
        [self addSubview:lab_title];
        
        NSString *str_description = [NSString stringWithFormat:@"%@", [NSMutableString ff_attributedStringWithString:@"规范化信息平台" space:2.0]];
        lab_description = [[FFLab alloc] initWithText:@"规范化信息平台" txtColor:FFColorFromHex(0xf9f9f9) txtAli:NSTextAlignmentCenter font:FFFontSize(14*ss)];
        [self addSubview:lab_description];
        
        v_lineL = [[UIView alloc] initWithFrame:CGRectZero];
        v_lineL.backgroundColor = FFColorFromHex(0xf9f9f9);
        [self addSubview:v_lineL];
        
        v_lineR = [[UIView alloc] initWithFrame:CGRectZero];
        v_lineR.backgroundColor = FFColorFromHex(0xf9f9f9);
        [self addSubview:v_lineR];
        
//        NSString *str_fliter = [NSString stringWithFormat:@"%@", [NSMutableString ff_attributedStringWithString:@"筛选" space:2.0]];
        btn_fliter = [[FFBtn alloc] initWithTitle:@"筛选" titleColor:FFBaseColor titleFont:FFFontSize(15*ss) bgColor:FFClearColor target:self action:@selector(ff_btnFliterAction:)];
        btn_fliter.layer.cornerRadius = 17.5*ss;
        [self addSubview:btn_fliter];
        
        btn_search = [[FFBtn alloc] initWithFrame:CGRectZero];
        [btn_search setImage:FFImg(@"icon_search") forState:UIControlStateNormal];
        [btn_search addTarget:self action:@selector(ff_btnSearchAction:) forControlEvents:UIControlEventTouchUpInside];
        btn_search.layer.cornerRadius = 37*ss;
        [self addSubview:btn_search];

        v_btnArr = [[FFBtnArr alloc] initWithBtns:@[@"手功能规\n范化平台", @"最新消息", @"手功能康复\n学术动态"] imgs:@[@"icon_handPlatform", @"icon_handNews", @"icon_handDynamic"] w:SCREENW-9*ss*2];
        v_btnArr.delegate = self;
        [self addSubview:v_btnArr];
        
        lab_notice = [[FFLab alloc] initWithText:@" 尊敬的用户在未注册会员的情况请您有60分钟体验时间" txtColor:FFColorFromHex(0xe56b4a) txtAli:NSTextAlignmentLeft font:FFFontSize(10*ss)];
        lab_notice.backgroundColor = FFColorFromHex(0xffe9ad);
        [self addSubview:lab_notice];
        
        [self ff_layoutView];
    }
    return self;
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    [imgv_bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(CGRectGetWidth(self.frame), CGRectGetHeight(self.frame)));
    }];
    
    [imgv_logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top).offset(45*ss+20*ss);
        make.centerX.mas_equalTo(self.mas_centerX);
//        make.size.mas_equalTo(CGSizeMake(<#CGFloat width#>, <#CGFloat height#>));
    }];
    
    [lab_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(imgv_logo.mas_bottom).offset(11*ss);
        make.centerX.mas_equalTo(self.mas_centerX);
//        make.size.mas_equalTo(CGSizeMake(<#CGFloat width#>, <#CGFloat height#>));
    }];
    
    [lab_description mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lab_title.mas_bottom).offset(11*ss);
        make.centerX.mas_equalTo(self.mas_centerX);
        //        make.size.mas_equalTo(CGSizeMake(<#CGFloat width#>, <#CGFloat height#>));
    }];
    
    [v_lineL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(lab_description.mas_left).offset(-11*ss);
        make.centerY.mas_equalTo(lab_description.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(60*ss, 1));
    }];
    
    [v_lineR mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lab_description.mas_right).offset(11*ss);
        make.centerY.mas_equalTo(lab_description.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(60*ss, 1));
    }];
    
    [v_btnArr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top).offset(220*ss);
        make.left.mas_equalTo(self.mas_left).offset(9*ss);
        make.size.mas_equalTo(CGSizeMake(SCREENW-9*ss*2, 100*ss));
    }];
    
    [lab_notice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.mas_bottom).offset(-10*ss);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(SCREENW-9*2*ss, 30*ss));
    }];
    
    [btn_fliter mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(v_btnArr.mas_top).offset(-10*ss);
        make.left.mas_equalTo(v_btnArr.mas_left);
        make.size.mas_equalTo(CGSizeMake(70*ss, 37*ss));
    }];
    
    [btn_search mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(v_btnArr.mas_top).offset(-10*ss);
        make.right.mas_equalTo(v_btnArr.mas_right);
        make.size.mas_equalTo(CGSizeMake(37*ss, 37*ss));
    }];
}

#pragma mark - FFBtnArrDelegate Method

- (void)ff_tapWithIndex:(NSInteger)index
{
    [self.delegate ff_tapWithIndex:index];
}

#pragma mark - Btn Action Method

- (void)ff_btnFliterAction:(id)sender
{
    [self.delegate ff_btnFliterAction];
}

- (void)ff_btnSearchAction:(id)sender
{
    [self.delegate ff_btnSearchAction];
}
@end
