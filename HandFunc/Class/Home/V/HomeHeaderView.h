//
//  HomeHeaderView.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/16.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FFBtnArr.h"

@protocol HomeHeaderViewDelegate;

@interface HomeHeaderView : UIView

@property (nonatomic, assign) id<HomeHeaderViewDelegate>delegate;

- (instancetype)init;

@end

@protocol HomeHeaderViewDelegate <NSObject>

- (void)ff_btnSearchAction;
- (void)ff_btnFliterAction;

- (void)ff_tapWithIndex:(NSInteger)index;

@end
