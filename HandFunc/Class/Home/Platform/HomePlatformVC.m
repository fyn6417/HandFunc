//
//  HomePlatformVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/16.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "HomePlatformVC.h"

@interface HomePlatformVC ()<FFTitleBarDelegate>

@end

@implementation HomePlatformVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = FFWhiteColor;
    FFTitleBar *v_titleBar = [[FFTitleBar alloc] initWithTitle:@"平台" leftImg:FFImg(@"icon_back") rightName:@""];
    v_titleBar.delegate = self;
    v_titleBar.frame = CGRectMake(0, 0, SCREENW, 64*ss);
    [self.view addSubview:v_titleBar];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - FFTitleBarDelegate Method

- (void)ff_leftBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
