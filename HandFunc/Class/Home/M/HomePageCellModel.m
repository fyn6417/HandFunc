//
//  HomePageCellModel.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/18.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "HomePageCellModel.h"

@implementation HomePageCellModel

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper
{
    return @{@"ff_title" : @"title",
      @"ff_description" : @"description",
          @"ff_imgName" : @"img"};
}

@end
