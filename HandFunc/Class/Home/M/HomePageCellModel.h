//
//  HomePageCellModel.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/18.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomePageCellModel : NSObject

@property NSString *ff_title;
@property NSString *ff_description;
@property NSString *ff_imgName;
@property NSString *ff_isPopularize;

@end
