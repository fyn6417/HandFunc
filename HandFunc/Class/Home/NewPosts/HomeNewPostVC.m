//
//  HomeNewPostVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/16.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "HomeNewPostVC.h"

@interface HomeNewPostVC ()<FFTitleBarDelegate,UITableViewDelegate, UITableViewDataSource, HomeNewPostHeaderViewDelegate>
{
    FFTitleBar *v_titleBar;
    UITableView *table_content;
    
    NSArray *arr_infoTitle;//一级菜单
    NSArray *arr_infoDetail;//二级菜单
    NSArray *arr_list;
}
@end

@implementation HomeNewPostVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFColorFromHex(0xaaaaaa);
    
    arr_infoTitle = @[@"科学前沿", @"文献分享", @"学术活动"];
    arr_infoDetail = @[@"手功能继续教育学习班", @"手功能学术会议", @"手功能年会"];
    arr_list = @[@{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"Icon40"},
                 @{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"Icon40"},
                 @{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"Icon40"},
                 @{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"Icon40"},
                 @{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"Icon40"},
                 @{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"Icon40"}];
    
}
#pragma mark - Build View Method

- (void)ff_buildView
{
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"详细栏目" leftImg:FFImg(@"icon_back") rightName:@""];
    v_titleBar.delegate = self;
    [self.view addSubview:v_titleBar];
    
    table_content = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    table_content.delegate = self;
    table_content.dataSource = self;
    table_content.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    table_content.backgroundColor = FFBaseBGColor;
    [self.view addSubview:table_content];
    
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    
    [v_titleBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(64*ss);
    }];
    
    [table_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(v_titleBar.mas_bottom);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREENW, SCREENH-49*ss-64*ss));
    }];

}

#pragma mark - FFTitleBarDelegate Method

- (void)ff_leftBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 200*ss;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    HomeNewPostHeaderView *view = [[HomeNewPostHeaderView alloc] initWithBtnArr:@[@"1", @"2", @"3"]];
    view.frame = CGRectMake(0, 0, SCREENW, 200);
    view.delegate = self;
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cID = @"cid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cID];
    }
    
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;

    HomePageCellModel *model = [HomePageCellModel yy_modelWithDictionary:arr_list[row]];
    HomePageCellView *view = [[HomePageCellView alloc] initWithModel:model];
    view.frame = cell.contentView.bounds;
    [cell.contentView addSubview:view];
    
//    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
}

#pragma mark - HomePostHeaderViewDelegate Method

- (void)ff_tapClass
{
    HandClassRegisterVC *vc = [HandClassRegisterVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)ff_tapOne:(NSInteger)index
{
    switch (index) {
        case 0:
            //
            NSLog(@"0");
            break;
            
        case 1:
            //
            NSLog(@"1");
            break;
            
        case 2:
            //
            NSLog(@"2");
            break;
    }
}

- (void)ff_tapTwo:(NSInteger)index
{
    switch (index) {
        case 0:
            //
            NSLog(@"00");
            break;
            
        case 1:
            //
            NSLog(@"01");
            break;
            
        case 2:
            //
            NSLog(@"02");
            break;
    }
}

@end
