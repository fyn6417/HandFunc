//
//  HomeNewPostHeaderView.m
//  HandFunc
//
//  Created by ff'Mac on 2017/8/2.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "HomeNewPostHeaderView.h"

@interface HomeNewPostHeaderView()<FFBtnArrDelegate>
{
    FFBtnArr *btn_arr1;
    FFBtnArr *btn_arr2;
    UIView *v_btnBG;
    UIImageView *imgv_class;
}

@end

@implementation HomeNewPostHeaderView

- (instancetype)initWithBtnArr:(NSArray *)btnArr
{
    self = [super init];
    if (self) {
        if (!btnArr) {
            return nil;
        }
        
        self.userInteractionEnabled = YES;
        
        btn_arr1 = [[FFBtnArr alloc] initWithBtns:btnArr w:SCREENW];
        btn_arr1.delegate = self;
        [self addSubview:btn_arr1];
        
        [btn_arr1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top);
            make.left.mas_equalTo(self.mas_left);
            make.right.mas_equalTo(self.mas_right);
            make.height.mas_equalTo(70);
        }];
        
        btn_arr2 = [[FFBtnArr alloc] initWithRoundBtns:btnArr w:SCREENW];
        btn_arr2.delegate = self;
        [self addSubview:btn_arr2];
        
        [btn_arr1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(btn_arr1.mas_bottom).offset(5);
            make.left.mas_equalTo(self.mas_left);
            make.right.mas_equalTo(self.mas_right);
            make.height.mas_equalTo(30);
        }];
        
//        for (NSInteger i = 0; i < btnArr.count; i++) {
//            FFBtn *btn = [[FFBtn alloc] initWithTitle:btnArr[i] titleColor:FFBaseBGColor titleFont:FFFontSize(14) bgColor:FFColorFromHex(0xaaaaaa) target:self action:@selector(ff_btnClicked:)];
//            [btn setTitleColor:FFBaseColor forState:UIControlStateSelected];
//            btn.tag = i;
//            CGFloat w = SCREENW/btnArr.count;
//            btn.frame = CGRectMake(i*w, 0, w, 100);
//            btn.layer.borderWidth = 0.3;
//            btn.layer.borderColor = FFColorFromHex(0xd2d2d2).CGColor;
//            [self addSubview:btn];
//            
//            
//            FFBtn *btnR = [[FFBtn alloc] initWithTitle:btnArr[i] titleColor:FFBaseBGColor titleFont:FFFontSize(14) bgColor:FFColorFromHex(0xaaaaaa) target:self action:@selector(ff_btnClicked:)];
//            btnR.tag = i;
//            [btnR setTitleColor:FFBaseColor forState:UIControlStateSelected];
////            CGFloat w = SCREENW/btnArr.count;
//            btnR.frame = CGRectMake(i*w, 110, w, 100);
//            btnR.layer.borderWidth = 0.3;
//            btnR.layer.borderColor = FFColorFromHex(0xd2d2d2).CGColor;
//            [self addSubview:btnR];
//        }
        
        
        
//        v_btnBG = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(btn_arr2.frame), SCREENW, 200)];
        imgv_class = [[UIImageView alloc] initWithFrame:CGRectMake(0, 100, SCREENW, 200)];
        imgv_class.image = FFImg(@"Icon98");
        imgv_class.userInteractionEnabled = YES;
        [imgv_class addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ff_tapClass)]];
        [self addSubview:imgv_class];
    }
    return self;
}

- (void)ff_tapClass
{
    [self.delegate ff_tapClass];
}

- (void)ff_btnClicked:(UIButton *)sender
{
    [self.delegate ff_tapOne:sender.tag];
}
@end
