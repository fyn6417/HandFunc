//
//  HomeNewPostHeaderView.h
//  HandFunc
//
//  Created by ff'Mac on 2017/8/2.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomeNewPostHeaderViewDelegate;

@interface HomeNewPostHeaderView : UIView

@property (nonatomic, assign)id<HomeNewPostHeaderViewDelegate>delegate;

- (instancetype)initWithBtnArr:(NSArray *)btnArr;

@end

@protocol HomeNewPostHeaderViewDelegate <NSObject>

- (void)ff_tapOne:(NSInteger)index;
- (void)ff_tapTwo:(NSInteger)index;

- (void)ff_tapClass;

@end
