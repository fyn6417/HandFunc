//
//  PayBillVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/20.
//  Copyright © 2017年 ff. All rights reserved.
//  支付

#import "PayBillVC.h"

@interface PayBillVC ()<FFTitleBarDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
{
    FFTitleBar *v_titleBar;
    UITableView *table_content;
    FFBtn *btn_submit;
    
    NSArray *arr_infoTitle;
    NSArray *arr_billInfo;
}
@end

@implementation PayBillVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFColorFromHex(0xaaaaaa);
    arr_billInfo = @[@"(申请发票成功之后出现此栏目)", @"20123123891083091", @"RMB 99.00元", @"张三", @"若各文化（大连）有限公司", @"辽宁省大连市高新区腾讯大厦", @"15102477737"];
    arr_infoTitle = @[@[@"费用金额"], @[@{@"ff_icon" : @"Icon20", @"ff_title" : @"微信支付"}, @{@"ff_icon" : @"Icon20", @"ff_title" : @"支付宝支付"}], arr_billInfo];
}
#pragma mark - Build View Method

- (void)ff_buildView
{
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"学习班注册" leftImg:FFImg(@"icon_back") rightImg:FFImg(@"")];
    v_titleBar.delegate = self;
    [self.view addSubview:v_titleBar];
    
    table_content = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    table_content.delegate = self;
    table_content.dataSource = self;
    table_content.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    table_content.backgroundColor = FFBaseBGColor;
    [self.view addSubview:table_content];
    
    btn_submit = [[FFBtn alloc] initWithTitle:@"确认支付" titleColor:FFWhiteColor titleFont:FFFontSize(15*ss) bgColor:FFBaseColor target:self action:@selector(ff_btnSubmit)];
    [self.view addSubview:btn_submit];
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    
    [v_titleBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(64*ss);
    }];
    
    [table_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(v_titleBar.mas_bottom);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREENW, SCREENH-49*ss-74*ss));
    }];
    
    [btn_submit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(table_content.mas_bottom).offset(40*ss);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(SCREENW-35*ss, 70*ss));
    }];
    
}

#pragma mark - Btn Action Method

- (void)ff_btnSubmit
{
    PayBillVC *vc = [PayBillVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)ff_btnApplyBill
{
    ApplyBillVC *vc = [ApplyBillVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - FFTitleBarDelegate Method

-(void)ff_leftBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arr_infoTitle.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return 2;
    } else {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return fKeyRowH;
    } else if (indexPath.section == 1) {
        return 50*ss;
    } else {
        return 200;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view;
    if (section == 2) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENW, 50*ss)];
        
        FFBtn *btn_applyBill = [[FFBtn alloc] initWithTitle:@"申请发票" titleColor:FFBaseColor titleFont:FFFontSize(12*ss) bgColor:FFClearColor target:self action:@selector(ff_btnApplyBill)];
        [view addSubview:btn_applyBill];
        
        [btn_applyBill mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(view.mas_centerX);
            make.centerY.mas_equalTo(view.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(200, 50));
        }];
        
        return view;
    } else {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENW, 50*ss)];
        return view;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 2) {
        return 50*ss;
    } else {
        return 10*ss;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cID = @"cid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cID];
    }
    
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
}

@end
