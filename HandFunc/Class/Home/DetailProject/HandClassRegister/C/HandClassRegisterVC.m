//
//  HandClassRegisterVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/20.
//  Copyright © 2017年 ff. All rights reserved.
//  报名详情页

#import "HandClassRegisterVC.h"

@interface HandClassRegisterVC ()<FFTitleBarDelegate, UITableViewDataSource, UITableViewDelegate, HandClassRegisterContentViewDelegate>
{
    FFTitleBar *v_titleBar;
    UITableView *table_content;
    NSArray *arr_content;
    NSDictionary *dic_content;
    
    HandClassRegisterContentModel *model_content;
}

@end

@implementation HandClassRegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFWhiteColor;
    dic_content = @{@"ff_title" : @"手功能康复学习", @"ff_img" : @"Icon20", @"ff_detailDescription" : @"手功能康复学习手功能康复学习手功能康复学习手功能康复学习手功能康复学习手功能康复学习手功能康复学习手功能康复学习手功能康复学习手功能康复学习"};
    arr_content = @[dic_content];
    model_content = [HandClassRegisterContentModel yy_modelWithDictionary:dic_content];
}

#pragma mark - Build View Method

- (void)ff_buildView
{
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"学习班报名" leftImg:FFImg(@"Icon20") rightName:@""];
    v_titleBar.delegate = self;
    [self.view addSubview:v_titleBar];
    
    table_content = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    table_content.delegate = self;
    table_content.dataSource = self;
    table_content.tableFooterView = [UIView new];
    [self.view addSubview:table_content];
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    [v_titleBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(64*ss);
    }];
    
    [table_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(v_titleBar.mas_bottom);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREENW, SCREENH-64*ss));
    }];
}

#pragma mark - UITableViewDelegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arr_content.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return fKeyRowH;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10*ss;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cID = @"cid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cID];
    }
    
    HandClassRegisterContentView *view = [[HandClassRegisterContentView alloc] initWithModel:model_content];
    view.delegate = self;
    [cell.contentView addSubview:view];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
}

#pragma mark - HandClassRegisterContentViewDelegate Method

-(void)ff_btnAction
{
    SubmitInfoVC *vc = [SubmitInfoVC new];
    [self.navigationController pushViewController:vc animated:YES];
}


@end
