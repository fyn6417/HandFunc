//
//  HandClassRegisterContentView.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/24.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HandClassRegisterContentModel.h"

@protocol HandClassRegisterContentViewDelegate;

@interface HandClassRegisterContentView : UIView

@property (nonatomic, assign) id<HandClassRegisterContentViewDelegate>delegate;

- (instancetype)initWithModel:(HandClassRegisterContentModel *)model;

@end

@protocol HandClassRegisterContentViewDelegate <NSObject>

- (void)ff_btnAction;

@end
