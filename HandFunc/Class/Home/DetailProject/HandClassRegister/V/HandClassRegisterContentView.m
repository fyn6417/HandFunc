//
//  HandClassRegisterContentView.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/24.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "HandClassRegisterContentView.h"

@interface HandClassRegisterContentView()
{
    FFLab *lab_title;
    UIImageView *imgv_content;
    FFLab *lab_description;
    
    FFBtn *btn_submit;
}

@end

@implementation HandClassRegisterContentView

- (instancetype)initWithModel:(HandClassRegisterContentModel *)model
{
    self = [super init];
    if (self) {
        if (model) {
            lab_title = [[FFLab alloc] initWithText:model.ff_title txtColor:FFColorFromHex(0x414141) txtAli:NSTextAlignmentLeft font:FFBoldFontSize(20*ss)];
            [self addSubview:lab_title];
            
            imgv_content = [[UIImageView alloc] initWithFrame:CGRectZero];
            imgv_content.image = FFImg(model.ff_img);
            [self addSubview:imgv_content];
            
            lab_description = [[FFLab alloc] initWithText:model.ff_detailDescription txtColor:FFColorFromHex(0xaaaaaa) txtAli:NSTextAlignmentLeft font:FFFontSize(12*ss)];
            lab_description.numberOfLines = 0;
            [self addSubview:lab_description];
            
            btn_submit = [[FFBtn alloc] initWithTitle:@"提  交" titleColor:FFWhiteColor titleFont:FFFontSize(16*ss) bgColor:FFBaseColor target:self action:@selector(ff_btnClicked)];
            [self addSubview:btn_submit];
            
            [self ff_layoutView];
        }
    }
    return self;
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    [lab_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top).offset(25*ss);
        make.left.mas_equalTo(self.mas_left).offset(18*ss);
    }];
    
    [imgv_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lab_title.mas_bottom).offset(25*ss);
        make.left.mas_equalTo(lab_title.mas_left);
        make.size.mas_equalTo(CGSizeMake(SCREENW-18*2*ss, 81*ss));
    }];
    
    [lab_description mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(imgv_content.mas_bottom).offset(16*ss);
        make.left.mas_equalTo(lab_title.mas_left);
        make.width.mas_equalTo(SCREENW-18*2*ss);
    }];
    
    [btn_submit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lab_description.mas_bottom).offset(40*ss);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(SCREENW-35*ss, 70*ss));
    }];
}

#pragma mark - Btn Action Method

- (void)ff_btnClicked
{
    [self.delegate ff_btnAction];
}

@end
