//
//  ApplyBillVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/20.
//  Copyright © 2017年 ff. All rights reserved.
//  申请发票

#import "ApplyBillVC.h"

@interface ApplyBillVC ()<FFTitleBarDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
{
    FFTitleBar *v_titleBar;
    UITableView *table_content;
    FFBtn *btn_submit;
    
    NSArray *arr_info;
    
}
@end

@implementation ApplyBillVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFColorFromHex(0xaaaaaa);
    
    arr_info = @[@"姓名", @"单位", @"部门", @"职务", @"地区"];
}
#pragma mark - Build View Method

- (void)ff_buildView
{
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"学习班注册" leftImg:FFImg(@"icon_back") rightImg:FFImg(@"")];
    v_titleBar.delegate = self;
    [self.view addSubview:v_titleBar];
    
    table_content = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    table_content.delegate = self;
    table_content.dataSource = self;
    table_content.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    table_content.backgroundColor = FFBaseBGColor;
    [self.view addSubview:table_content];
    
    btn_submit = [[FFBtn alloc] initWithTitle:@"提  交" titleColor:FFWhiteColor titleFont:FFFontSize(15*ss) bgColor:FFBaseColor target:self action:@selector(ff_btnSubmit)];
    [self.view addSubview:btn_submit];
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    
    [v_titleBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(64*ss);
    }];
    
    [table_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(v_titleBar.mas_bottom);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREENW, SCREENH-49*ss-74*ss));
    }];
    
    [btn_submit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(table_content.mas_bottom).offset(40*ss);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(SCREENW-35*ss, 70*ss));
    }];
}

#pragma mark - Btn Action Method

- (void)ff_btnSubmit
{
//    PayBillVC *vc = [PayBillVC new];
//    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - FFTitleBarDelegate Method

- (void)ff_leftBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arr_info.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return fKeyRowH;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10*ss;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cID = @"cid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cID];
    }
    
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
}

@end
