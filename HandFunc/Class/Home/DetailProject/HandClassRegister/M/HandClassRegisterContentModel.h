//
//  HandClassRegisterContentModel.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/24.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HandClassRegisterContentModel : NSObject

@property NSString *ff_title;
@property NSString *ff_img;
@property NSString *ff_detailDescription;

@end

@interface HandClassRegisterSubmitInfoModel : NSObject

@property NSString *ff_name;
@property NSString *ff_company;
@property NSString *ff_dep;
@property NSString *ff_telNum;

@end

@interface HandClassRegisterApplyBillInfoModel : NSObject

@property NSString *ff_bTitle;
@property NSString *ff_bTaxiNum;
@property NSString *ff_bPrice;
@property NSString *ff_bName;
@property NSString *ff_bCompany;
@property NSString *ff_bCompanyAddr;
@property NSString *ff_bTelNum;

@end
