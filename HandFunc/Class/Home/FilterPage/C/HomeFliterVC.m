//
//  HomeFliterVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/16.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "HomeFliterVC.h"

@interface HomeFliterVC ()<FFTitleBarDelegate, UITableViewDataSource, UITableViewDelegate>
{
    FFTitleBar *v_titleBar;
    
    UITableView *table_l;
    UITableView *table_m;
    UITableView *table_r;
    
    NSArray *arr_content;
}
@end

@implementation HomeFliterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Config Method

- (void)ff_config
{
    self.navigationController.navigationBar.hidden = YES;
    arr_content = @[@[@"手功能康复\n规范化评估", @"手功能康复\n规范化治疗技术", @"手功能康复\n辅助技术", @"手功能康复\n新技术", @"手功能康复\n标准化建设",@"手功能康复\n规范方案"],
                    @[@"形态评估", @"神经功能评估", @"感觉功能评估", @"运动功能评估", @"日常生活评估"],
                    @[@"全部", @"外观", @"手部组织", @"手部颜色", @"皮肤状态", @"骨骼", @"软组织", @"脉管", @"肌肉"]];
}

#pragma mark - Build View Method

- (void)ff_buildView
{
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"栏目筛选" leftImg:FFImg(@"icon_back") rightName:@""];
    v_titleBar.delegate = self;
    [self.view addSubview:v_titleBar];
    
    table_l = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    table_l.delegate = self;
    table_l.dataSource = self;
    table_l.backgroundColor = FFColorFromHex(0xd2d2d2);
    table_l.layer.borderWidth = .3f;
    table_l.layer.borderColor = FFColorFromHex(0xf5f5f5).CGColor;
    table_l.separatorInset = UIEdgeInsetsMake(0, -20, 0, 0);
    [self.view addSubview:table_l];
    
    table_m = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    table_m.delegate = self;
    table_m.dataSource = self;
    table_m.backgroundColor = [UIColor lightGrayColor];
    table_m.layer.borderWidth = .3f;
    table_m.layer.borderColor = FFColorFromHex(0xf5f5f5).CGColor;
    table_m.separatorInset = UIEdgeInsetsMake(0, -20, 0, 0);
    [self.view addSubview:table_m];
    
    table_r = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    table_r.delegate = self;
    table_r.dataSource = self;
    table_r.backgroundColor = FFColorFromHex(0xebebeb);
    table_r.layer.borderWidth = .3f;
    table_r.layer.borderColor = FFColorFromHex(0xf5f5f5).CGColor;
    table_r.separatorInset = UIEdgeInsetsMake(0, -20, 0, 0);
    [self.view addSubview:table_r];
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    [v_titleBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(64*ss);
    }];
    
    [table_l mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(v_titleBar.mas_bottom);
        make.left.mas_equalTo(self.view.mas_left);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREENW/3, SCREENH-64*ss));
    }];
    
    [table_m mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(v_titleBar.mas_bottom);
        make.left.mas_equalTo(table_l.mas_right);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREENW/3, SCREENH-64*ss));
    }];
    
    [table_r mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(v_titleBar.mas_bottom);
        make.left.mas_equalTo(table_m.mas_right);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREENW/3, SCREENH-64*ss));
    }];
}

#pragma mark - FFTitleBarDelegate Method

- (void)ff_leftBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table_l) {
        return [arr_content[0] count];
    } else if (tableView == table_m) {
        return [arr_content[1] count];
    } else {
        return [arr_content[2] count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65*ss;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .1*ss;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cID = @"cid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cID];
    }
    
//    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (tableView == table_l) {
        FFLab *lab = [[FFLab alloc] initWithText:[arr_content[0] objectAtIndex:row] txtColor:row == 0 ? FFBaseColor : FFColorFromHex(0x414141) txtAli:NSTextAlignmentLeft font:FFFontSize(12*ss)];
        [cell.contentView addSubview:lab];
        
        [lab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(cell.contentView.mas_centerX);
            make.centerY.mas_equalTo(cell.contentView.mas_centerY);
//            make.size.mas_equalTo(CGSizeMake(<#CGFloat width#>, <#CGFloat height#>));
        }];
    } else if (tableView == table_m) {
        FFLab *lab = [[FFLab alloc] initWithText:[arr_content[1] objectAtIndex:row] txtColor:row == 0 ? FFBaseColor : FFColorFromHex(0x414141) txtAli:NSTextAlignmentLeft font:FFFontSize(12*ss)];
        [cell.contentView addSubview:lab];
        
        [lab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(cell.contentView.mas_centerX);
            make.centerY.mas_equalTo(cell.contentView.mas_centerY);
            //            make.size.mas_equalTo(CGSizeMake(<#CGFloat width#>, <#CGFloat height#>));
        }];
    } else if (tableView == table_r) {
        FFLab *lab = [[FFLab alloc] initWithText:[arr_content[2] objectAtIndex:row] txtColor:row == 0 ? FFBaseColor : FFColorFromHex(0x414141) txtAli:NSTextAlignmentLeft font:FFFontSize(12*ss)];
        [cell.contentView addSubview:lab];
        
        [lab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(cell.contentView.mas_centerX);
            make.centerY.mas_equalTo(cell.contentView.mas_centerY);
            //            make.size.mas_equalTo(CGSizeMake(<#CGFloat width#>, <#CGFloat height#>));
        }];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
}

@end
