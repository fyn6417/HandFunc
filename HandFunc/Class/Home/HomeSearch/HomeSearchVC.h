//
//  HomeSearchVC.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/16.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchInputtingDelegate <NSObject>

- (void)searchMyInput:(NSString *)inputStr;

@end

@interface HomeSearchVC : UIViewController

@property(nonatomic,weak)id<SearchInputtingDelegate>delegate;

@end
