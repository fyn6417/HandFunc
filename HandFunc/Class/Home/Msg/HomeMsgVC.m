//
//  HomeMsgVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/16.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "HomeMsgVC.h"

@interface HomeMsgVC ()<FFTitleBarDelegate, UITableViewDelegate, UITableViewDataSource,FFBtnArrDelegate>
{
    //    FFMePersonModel *ff_model;
    UITableView *table_person;
    FFTitleBar *v_titleBar;
    FFBtnArr *btn_arr;
    NSArray *arr_infoTitle;
    NSArray *arr_list;
}
@end

@implementation HomeMsgVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFColorFromHex(0xaaaaaa);
    
    arr_infoTitle = @[@"实时新闻", @"置顶文章", @"热门文章", @"佳文分享"];
    arr_list = @[@{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"Icon40"},
                 @{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"Icon40"},
                 @{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"Icon40"},
                 @{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"Icon40"},
                 @{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"Icon40"},
                 @{@"title" : @"患者招募", @"description" : @"手是人类最为重要的劳动器官", @"img" : @"Icon40"}];
}
#pragma mark - Build View Method

- (void)ff_buildView
{
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"新闻" leftImg:FFImg(@"icon_back") rightImg:FFImg(@"")];
    v_titleBar.delegate = self;
    [self.view addSubview:v_titleBar];
    
    btn_arr = [[FFBtnArr alloc] initWithBtns:arr_infoTitle w:SCREENW];
    btn_arr.delegate = self;
    [self.view addSubview:btn_arr];
    
    table_person = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    table_person.delegate = self;
    table_person.dataSource = self;
    table_person.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    table_person.backgroundColor = FFBaseBGColor;
    [self.view addSubview:table_person];
    
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    
    [v_titleBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(64*ss);
    }];
    
    [btn_arr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(v_titleBar.mas_bottom);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(100);
    }];
    
    [table_person mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(btn_arr.mas_bottom);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREENW, SCREENH-49*ss-74*ss));
    }];
}


#pragma mark - UITableViewDelegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arr_list.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10*ss;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cID = @"cid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cID];
    }
    
    NSInteger row = indexPath.row;
    HomePageCellModel *model = [HomePageCellModel yy_modelWithDictionary:arr_list[row]];
    HomePageCellView *view = [[HomePageCellView alloc] initWithModel:model];
    view.frame = cell.contentView.bounds;
    [cell.contentView addSubview:view];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
}

#pragma mark - FFTitleBarDelegate Method

- (void)ff_leftBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - FFBtnArrDelegate Method

- (void)ff_tapWithIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            //
            break;
        
        case 1:
            //
            break;
        case 2:
            //
            break;
        case 3:
            //
            break;
    }
    [table_person reloadData];
}
@end
