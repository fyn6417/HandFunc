//
//  MainTabBarVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/6/28.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "MainTabBarVC.h"

@interface MainTabBarVC ()

@end

@implementation MainTabBarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_setupVC];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Build View Method

- (void)ff_setupVC
{
    
    NSArray *arr_tabBar = @[@"HomePageVC", @"ForumVC", @"FFMeVC"];//@"SubmitPaperVC"
    NSArray *arr_titleTabBar = @[@"首页", @"论坛", @"我的"];
    NSArray *arr_imgTabBar = @[@"homePage", @"forumPage", @"mePage"];
    NSArray *arr_imgTabBarSelected = @[@"homePage", @"forum_selected", @"mePage_selected"];
    
    for (NSInteger i = 0; i < arr_tabBar.count; i++) {
        Class aClass = NSClassFromString(arr_tabBar[i]);
        UIViewController *vc = [aClass new];

        vc.navigationItem.title = arr_titleTabBar[i];
        UINavigationController *nav_home = [[UINavigationController alloc]initWithRootViewController:vc];
        UIImage *img = [UIImage ff_imgWithOriginalImg:arr_imgTabBar[i]];
        UIImage *selectedImg = [UIImage ff_imgWithOriginalImg:arr_imgTabBarSelected[i]];
        UITabBarItem *tabItem = [[UITabBarItem alloc] initWithTitle:arr_titleTabBar[i] image:img selectedImage:selectedImg];
        nav_home.tabBarItem = tabItem;
        [self addChildViewController:nav_home];
    }
    self.tabBar.tintColor = FFBaseColor;
    self.selectedIndex = 0;
}

@end
