//
//  ForumCommentVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/8.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "ForumCommentVC.h"

@interface ForumCommentVC ()<UITextFieldDelegate, FFTitleBarDelegate>
{
    FFTitleBar *v_titleBar;
    UITextField *tf_comment;
}
@end

@implementation ForumCommentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFWhiteColor;
}

#pragma mark - Build View Method

- (void)ff_buildView
{
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"评  论" leftImg:FFImg(fKeyBackBtnImg) rightName:@"提交"];
    v_titleBar.delegate = self;
    [self.view addSubview:v_titleBar];
    
    tf_comment = [[UITextField alloc] initWithFrame:CGRectZero];
    tf_comment.delegate = self;
    tf_comment.placeholder = @"请输入文字内容...";
    tf_comment.backgroundColor = FFColorFromHex(0xaaaaaa);
    [self.view addSubview:tf_comment];
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    [v_titleBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(64*ss);
    }];
    
    [tf_comment mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(v_titleBar.mas_bottom).offset(2*ss);
        make.left.mas_equalTo(self.view.mas_left).offset(2*ss);
        make.right.mas_equalTo(self.view.mas_right).offset(2*ss);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(2*ss);
    }];
}

#pragma mark - FFTitleBarDelegate Method

- (void)ff_leftBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)ff_rightBtnClicked
{
    //提交
}
@end
