//
//  ForumDetailVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/8.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "ForumDetailVC.h"

@interface ForumDetailVC ()<FFTitleBarDelegate, ForumCellDelegate, UITableViewDelegate, UITableViewDataSource>
{
    FFTitleBar *v_titleBar;
    UITableView *table_content;
    
    NSMutableArray *mArr_content;
}
@end

@implementation ForumDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFBaseBGColor;
}

#pragma mark - Build View Method

- (void)ff_buildView
{
    
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"详细栏目" leftImg:FFImg(fKeyBackBtnImg) rightName:@""];
    [self.view addSubview:v_titleBar];
    
    table_content = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    table_content.delegate = self;
    table_content.dataSource = self;
    [self.view addSubview:table_content];
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    [table_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).offset(64*ss+5*ss);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.bottom.mas_equalTo(self.view.mas_bottom);
    }];
}

#pragma mark - FFTitleBarDelegate Method

- (void)ff_leftBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)ff_rightBtnClicked
{
    //搜索
}

#pragma mark - UITableViewDelegate Method

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    ForumListModel *model = [ForumListModel yy_modelWithDictionary:[mArr_content objectAtIndex:indexPath.row]];
//    NSInteger h = model.imgNameArr.count%3;
    //根据model计算cell高度
    return 200;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cID = @"cID";
    ForumCell *cell = [tableView dequeueReusableCellWithIdentifier:cID];
    if (cell == nil) {
        cell = [[ForumCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cID];
    }
    cell.delegate = self;
    ForumListModel *model = [ForumListModel yy_modelWithDictionary:[mArr_content objectAtIndex:indexPath.row]];
    [cell ff_setDataWithModel:model];
    return cell;
}

#pragma mark - UITableViewDataSource Method

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return mArr_content.count;
}

#pragma mark - ForumCellDelegate Method

- (void)ff_like
{

}

- (void)ff_comment
{

}

- (void)ff_tapImgWithIndex:(NSInteger)index
{

}

@end

