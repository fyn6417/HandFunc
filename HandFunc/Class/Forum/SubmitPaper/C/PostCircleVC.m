//
//  PostCircleVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/9.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "PostCircleVC.h"

@interface PostCircleVC ()
{
    FFBtn *btn_submit;
    FFTitleBar *v_titleBar;
}
@end

@implementation PostCircleVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Build View Method

- (void)ff_buildView
{
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"投稿通道" leftImg:FFImg(fKeyBackBtnImg) rightName:@""];
    [self.view addSubview:v_titleBar];
    
    btn_submit = [[FFBtn alloc] initWithTitle:@"提  交" titleColor:FFWhiteColor titleFont:FFFontSize(12*ss) bgColor:FFBaseColor  target:self action:@selector(ff_btnAction)];
    [self.view addSubview:btn_submit];
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    [btn_submit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_bottom).offset(10*ss);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        //        make.size.mas_equalTo(CGSizeMake(<#CGFloat width#>, <#CGFloat height#>));
    }];
}

#pragma mark - Btn Action Method

- (void)ff_btnAction
{
    
}

@end
