//
//  SubmitPaperVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/9.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "SubmitPaperVC.h"

@interface SubmitPaperVC ()<FFTitleBarDelegate, UITableViewDelegate, UITableViewDataSource>
{
    FFBtn *btn_submit;
    FFTitleBar *v_titleBar;
    UITableView *table_content;
}
@end

@implementation SubmitPaperVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFColorFromHex(0xaaaaaa);
}

#pragma mark - Build View Method

- (void)ff_buildView
{
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"动态发布" leftImg:FFImg(fKeyBackBtnImg) rightName:@""];
    v_titleBar.delegate = self;
    [self.view addSubview:v_titleBar];
    
    btn_submit = [[FFBtn alloc] initWithTitle:@"提  交" titleColor:FFWhiteColor titleFont:FFFontSize(12*ss) bgColor:FFBaseColor target:self action:@selector(ff_btnAction)];
    [self.view addSubview:btn_submit];
    
    table_content = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    table_content.delegate = self;
    table_content.dataSource = self;
    table_content.backgroundColor = FFBaseBGColor;
    table_content.tableFooterView = [UIView new];
    table_content.scrollEnabled = NO;
    
    [self.view addSubview:table_content];
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    [v_titleBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(64*ss);
    }];
    
    [table_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(v_titleBar.mas_bottom);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(SCREENW, 200+30+fKeyRowH*2));
    }];
    
    [btn_submit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(table_content.mas_bottom).offset(10);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(SCREENW-40*ss, 40));
    }];
}

#pragma mark - FFTitleBarDelegate Method

-(void)ff_leftBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat section = indexPath.section;
    if (section == 0) {
        return 200*ss;
    } else {
        return fKeyRowH;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10*ss;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cID = @"cid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cID];
    }
    
    NSInteger section = indexPath.section;
//    NSInteger row = indexPath.row;
    
    if (section == 0) {
        //
    } else if (section == 1) {
        //
    } else {
        //
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
}

#pragma mark - Btn Action Method

- (void)ff_btnAction
{
    
}


@end
