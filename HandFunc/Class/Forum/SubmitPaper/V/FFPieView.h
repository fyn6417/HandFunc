//
//  FFPieView.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/3.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FFPieView : UIView

- (id)initWithFrame:(CGRect)frame dataItems:(NSArray *)dataItems colorItems:(NSArray *)colorItems;
- (void)stroke;

@end
