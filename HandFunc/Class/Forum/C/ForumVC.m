//
//  ForumVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/6/28.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "ForumVC.h"

@interface ForumVC ()<FFBtnArrDelegate, UITableViewDelegate, UITableViewDataSource, ForumCellDelegate>
{
    UITableView *tableV_forum;
    FFBtnArr *view_btnArr;
    
    FFPieView *pie;
    BOOL isLiked;
    NSMutableArray *mArr_List;
}

@end

@implementation ForumVC

#pragma mark - Life Circle Method

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self ff_getCode];
    [self ff_config];
    [self ff_buildView];
//    [self ff_checkLoginState];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Check Login Method

- (void)ff_checkLoginState
{
    if (![FFUtils ff_isLogin]) {
        //未登录
        [self presentViewController:[LoginVC new] animated:YES completion:nil];
    }
}

#pragma mark - Config Method

- (void)ff_config
{
    isLiked = NO;
}

#pragma mark - Build View Method

- (void)ff_buildView
{
    [self.view addSubview:[self tableV_forum]];
    [self.view addSubview:[self view_btnArr]];
    
    pie = [[FFPieView alloc] initWithFrame:CGRectMake(SCREENW-120*ss, SCREENH-170*ss, 100*ss, 100*ss) dataItems:@[@"1", @"1"] colorItems:@[FFGreenColor, FFBaseColor]];
    
//    [self.view addSubview:pie];
    [pie stroke];
    
    [tableV_forum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(view_btnArr.mas_bottom);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-49*ss);
    }];
}


#pragma mark - UITableViewDataSource Method

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return mArr_List.count;
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200*ss;
}

#pragma mark - UITableViewDelegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cID = @"cid";
    ForumCell *cell = [tableView dequeueReusableCellWithIdentifier:cID];
    if (!cell) {
        cell = [[ForumCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cID];
    }
    cell.delegate = self;
    ForumListModel *model = [ForumListModel yy_modelWithDictionary:[mArr_List objectAtIndex:indexPath.row]];
    [cell ff_setDataWithModel:model];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self ff_pushToDetailVCWithID:@"1"];
}

- (void)ff_pushToDetailVCWithID:(NSString *)ff_id
{
    ForumDetailVC *vc = [ForumDetailVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - FFBtnArrDelegate Method

- (void)ff_tapWithIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            [self ff_pushTo];
            break;
        case 1:
            [self ff_pushTo];
            break;
        case 2:
            [self ff_pushTo];
            break;
        case 3:
            [self ff_pushTo];
            break;
        default:
            break;
    }
}

#pragma mark - PushToVC Method

- (void)ff_pushTo
{
    UIViewController *vc = [UIViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Lazy Loading Method

- (UITableView *)tableV_forum
{
    if (!tableV_forum) {
        tableV_forum = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        tableV_forum.delegate = self;
        tableV_forum.dataSource = self;
//        tableV_forum.backgroundColor = FFRedColor;
        tableV_forum.frame = CGRectZero;
    }
    UITableView *temp = tableV_forum;
    return temp;
}

- (FFBtnArr *)view_btnArr
{
    if (!view_btnArr) {
        view_btnArr = [[FFBtnArr alloc] initWithBtns:@[@"专家面对面", @"病例研讨", @"科研学术", @"会议活动"] imgs:@[@"icon_faceTo_disEnable", @"icon_discuss_disEnable", @"icon_scientific_disEnable", @"icon_conference_disEnable"] w:SCREENW];
        view_btnArr.frame = CGRectMake(0, 64*ss, SCREENW, 80*ss);
    }
    FFBtnArr *ret = view_btnArr;
    return ret;
}

#pragma mark - ForumCellDelegate Method

- (void)ff_like
{
    NSLog(@"Liked");
    if (isLiked) {
        
    }
    isLiked = !isLiked;
}

- (void)ff_comment
{
    NSLog(@"comment");
    ForumCommentVC *vc = [ForumCommentVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)ff_getCode
{
    [[FFNetwork sharedManager] ff_requsetWithMethod:GET path:@"https://www.bestguy.net/v1/public/news/list/1/1/1" params:nil success:^(NSDictionary *dic) {
        NSLog(@"%@", dic);
    } failure:^(NSError *err) {
        //
    }];
}

@end
