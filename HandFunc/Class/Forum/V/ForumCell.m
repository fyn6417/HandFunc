//
//  ForumCell.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/1.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "ForumCell.h"

@interface ForumCell()
{
    UIView *view_bg;
    
    UIImageView *imgv_head;
    UILabel *lab_name;
    UILabel *lab_date;
    UILabel *lab_timeStamp;
    
    UIButton *btn_reply;//回复
    UIButton *btn_subscription;//关注
    
    
    UILabel *lab_content;
    UICollectionView *collectionView;//照片组
    
    UIButton *btn_like;
    UIButton *btn_comment;
    
    UILabel *lab_likeCount;
    UILabel *lab_commentCount;
    NSAttributedString *str_addr;//地点
    UIImageView *imgv_lastComment;
    UILabel *lab_lastComment;
    
    /*
     1.person -> 头像、昵称
     2.日期
     3.时间
     4.回复
     5.关注
     6.论坛文字内容
     7.图片数组
     8.点赞数
     9.回复数
     10.有无地点信息
     11.最后一条评论内容
     12.最后一条评论人的name
     13.评论总数
     
     */
}

@end

@implementation ForumCell

#pragma mark - Life Circle Method

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        view_bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENW, 200*ss)];
        
        view_bg.backgroundColor = FFWhiteColor;
        [self.contentView addSubview:view_bg];
        
        imgv_head = [[UIImageView alloc] initWithFrame:CGRectMake(10*ss, 10*ss, 40*ss, 40*ss)];
        FFViewBorderRadius(imgv_head, imgv_head.frame.size.width, 0, FFClearColor);
        [view_bg addSubview:imgv_head];
        
        lab_name = [[UILabel alloc] initWithFrame:CGRectZero];
        lab_name.textColor = FFBaseColor;
        lab_name.textAlignment = NSTextAlignmentLeft;
        lab_name.font = FFFontSize(14*ss);
        [view_bg addSubview:lab_name];
        
        lab_date = [[UILabel alloc] initWithFrame:CGRectZero];
        lab_date.textColor = FFColorFromHex(0xd2d2d2);
        lab_date.textAlignment = NSTextAlignmentLeft;
        lab_date.font = FFFontSize(12*ss);
        [view_bg addSubview:lab_date];
        
        lab_timeStamp = [[UILabel alloc] initWithFrame:CGRectZero];
        lab_timeStamp.textColor = FFColorFromHex(0xd2d2d2);
        lab_timeStamp.textAlignment = NSTextAlignmentLeft;
        lab_timeStamp.font = FFFontSize(12*ss);
        [view_bg addSubview:lab_timeStamp];
        
        lab_content = [[UILabel alloc] initWithFrame:CGRectZero];
        lab_content.numberOfLines = 0;
        lab_content.textColor = FFColorFromHex(0x234566);
        lab_content.textAlignment = NSTextAlignmentLeft;
        lab_content.font = FFFontSize(15*ss);
        [view_bg addSubview:lab_content];
        
//        collectionView = [UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:<#(nonnull UICollectionViewLayout *)#>
        
        
        btn_like = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_like.frame = CGRectZero;
        [btn_like addTarget:self action:@selector(ff_btnLikeAction) forControlEvents:UIControlEventTouchUpInside];
        [view_bg addSubview:btn_like];
        
        btn_comment = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_comment.frame = CGRectZero;
        [btn_comment addTarget:self action:@selector(ff_btnCommentAction) forControlEvents:UIControlEventTouchUpInside];
        [view_bg addSubview:btn_comment];
        
        lab_likeCount = [[UILabel alloc] initWithFrame:CGRectZero];
        lab_likeCount.textColor = FFBaseColor;
        lab_likeCount.textAlignment = NSTextAlignmentLeft;
        lab_likeCount.font = FFFontSize(13*ss);
        [view_bg addSubview:lab_likeCount];
        
        lab_commentCount = [[UILabel alloc] initWithFrame:CGRectZero];
        lab_commentCount.textColor = FFBaseColor;
        lab_commentCount.textAlignment = NSTextAlignmentLeft;
        lab_commentCount.font = FFFontSize(13*ss);
        [view_bg addSubview:lab_commentCount];
        
        imgv_lastComment = [[UIImageView alloc] initWithFrame:CGRectZero];
        [imgv_lastComment addSubview:lab_lastComment];
        
        lab_lastComment = [[UILabel alloc] initWithFrame:CGRectZero];
        lab_lastComment.textColor = FFColorFromHex(0xf2f2f2);
        lab_lastComment.textAlignment = NSTextAlignmentLeft;
        lab_lastComment.font = FFFontSize(14*ss);
        [view_bg addSubview:lab_lastComment];
        
        [self ff_layoutSubviews];
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Layout Method

- (void)ff_layoutSubviews
{
    [imgv_head mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(view_bg.mas_top).offset(5*ss);
        make.left.mas_equalTo(view_bg.mas_left).offset(5*ss);
        make.size.mas_equalTo(CGSizeMake(40*ss, 40*ss));
    }];
    
    [lab_name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(view_bg.mas_top).offset(7*ss);
        make.left.mas_equalTo(imgv_head.mas_right).offset(5*ss);
//        make.size.mas_equalTo(CGSizeMake(80, 20));
    }];
    
    [lab_date mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lab_name.mas_bottom).offset(3*ss);
        make.left.mas_equalTo(lab_name.mas_left);
//        make.size.mas_equalTo(CGSizeMake(50, 20));
    }];
    
    [lab_timeStamp mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(lab_date.mas_centerY);
        make.left.mas_equalTo(lab_date.mas_right).offset(5*ss);
//        make.size.mas_equalTo(CGSizeMake(50, 20));
    }];
    
    [lab_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(imgv_head.mas_bottom).offset(10*ss);
        make.left.mas_equalTo(lab_name.mas_left);
//        make.size.mas_equalTo(CGSizeMake(SCREENW-60, 60));
        make.width.mas_equalTo(SCREENW-60*ss);
    }];
    
    [btn_like mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lab_content.mas_bottom).offset(20*ss);
        make.left.mas_equalTo(lab_content.mas_left);
//        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
 
    [btn_comment mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(btn_like.mas_centerY);
        make.left.mas_equalTo(btn_like.mas_right).offset(20*ss);
//        make.size.mas_equalTo(CGSizeMake(50, 30));
    }];
    
    [lab_likeCount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(btn_comment.mas_centerY);
        make.right.mas_equalTo(lab_commentCount.mas_left).offset(-10*ss);
    }];
    
    [lab_commentCount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(lab_likeCount.mas_centerY);
        make.right.mas_equalTo(view_bg.mas_right).offset(-20*ss);
    }];
}

#pragma mark - Fill Data Method

- (void)ff_setDataWithModel:(ForumListModel *)model
{
    imgv_head.image = FFImg(@"Icon40");
    lab_name.text = @"Koala";
    lab_date.text = @"07-02";
    lab_timeStamp.text = @"23:32";
    
    lab_content.text = @"今天是个好日子，17年就剩半年了，还有半年就过年了！";
    [btn_like setImage:FFImg(@"icon_liked") forState:UIControlStateNormal];
    [btn_comment setImage:FFImg(@"icon_comment") forState:UIControlStateNormal];
    
    lab_likeCount.text = @"赞27";
    lab_commentCount.text = @"回复127";
    
    lab_lastComment.text = @"别bb了，快去刷碗";
    self.rowHight = CGRectGetMaxY(btn_comment.frame)+30*ss;
}

#pragma mark - Btn Action Method

- (void)ff_btnLikeAction
{
    [self.delegate ff_like];
}

- (void)ff_btnCommentAction
{
    [self.delegate ff_comment];
}

@end
