//
//  ForumCell.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/1.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ForumListModel.h"

@protocol ForumCellDelegate;

@interface ForumCell : UITableViewCell

@property (nonatomic, assign) id<ForumCellDelegate>delegate;
@property (nonatomic, assign) CGFloat rowHight;


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;
- (void)ff_setDataWithModel:(ForumListModel *)model;

@end

@protocol ForumCellDelegate <NSObject>

@optional
- (void)ff_like;
- (void)ff_comment;
- (void)ff_tapImgWithIndex:(NSInteger)index;
- (void)ff_reply;
- (void)ff_subscription;
- (void)ff_personName;

@end
