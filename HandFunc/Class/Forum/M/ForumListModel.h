//
//  ForumListModel.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/2.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FFPerson : NSObject

@property NSString *name;
@property NSString *imgName;

@end

@interface ForumListModel : NSObject

@property FFPerson *personInfo;
@property NSString *date;
@property NSString *timeStamp;
@property NSString *content;
@property NSMutableArray *imgNameArr;
@property NSString *isLiked;
@property NSString *lastComment;
@property NSString *likeCount;
@property NSString *commentCount;

@end


