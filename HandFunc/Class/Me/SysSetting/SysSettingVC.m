//
//  SysSettingVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/13.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "SysSettingVC.h"

#define fHeightForRow 35*ss

@interface SysSettingVC ()<FFTitleBarDelegate, UITableViewDataSource, UITableViewDelegate>
{
    FFTitleBar *v_titleBar;
    UITableView *table_content;
    FFBtn *btn_logout;
    
    
    FFSwitch *s_newMsgNotice;
    FFSwitch *s_systemNoti;
    
    NSArray *arr;
}
@end

@implementation SysSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFBaseBGColor;
    
    arr = @[@"新消息提醒", @"通知推送", @"当前版本"];
    
}

#pragma mark - Build View Method

- (void)ff_buildView
{
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"系统设置" leftImg:FFImg(@"Icon40") rightName:@""];
    v_titleBar.delegate = self;
    [self.view addSubview:v_titleBar];
    
    table_content = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    table_content.delegate = self;
    table_content.dataSource = self;
    table_content.backgroundColor = FFBaseBGColor;
    table_content.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:table_content];
    
    if (![FFUtils ff_isLogin]) {
        btn_logout = [[FFBtn alloc] initWithTitle:@"退出登录" titleColor:FFWhiteColor titleFont:FFFontSize(15*ss) bgColor:FFBaseColor target:self action:@selector(ff_btnLogoutAction)];
        btn_logout.layer.cornerRadius = FFCorner;
        
        [self.view addSubview:btn_logout];
        
        [btn_logout mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.view.mas_bottom).offset(-100*ss);
            make.centerX.mas_equalTo(self.view.mas_centerX);
            make.size.mas_equalTo(CGSizeMake(SCREENW-40*ss, 35*ss));
        }];
    }
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    [v_titleBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(64*ss);
    }];
    
    [table_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(v_titleBar.mas_bottom).offset(10*ss);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREENW, (fKeyRowH+10)*arr.count*ss));
    }];
}

#pragma mark - Btn Action Method

- (void)ff_btnLogoutAction
{
    //退出登录
    NSLog(@"退出登录");
}

#pragma mark - FFTitleBarDelegate Method

- (void)ff_leftBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return fKeyRowH;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10*ss;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cID = @"cid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cID];
    }
    NSInteger section = indexPath.section;
    cell.textLabel.text = arr[section];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (section == 0) {
        CGSize size = CGSizeMake(50*ss, 30*ss);
        s_newMsgNotice = [[FFSwitch alloc] initWithFrame:CGRectMake(SCREENW-size.width-20*ss, fKeyRowH/2-size.height/2, size.width, size.height) OnColor:FFBaseColor on:YES target:self sel:@selector(ff_switchStatusChanged:)];
        [cell.contentView addSubview:s_newMsgNotice];
    } else if (section == 1) {
        CGSize size = CGSizeMake(50*ss, 30*ss);
        s_systemNoti = [[FFSwitch alloc] initWithFrame:CGRectMake(SCREENW-size.width-20*ss, fKeyRowH/2-size.height/2, size.width, size.height) OnColor:FFBaseColor on:YES target:self sel:@selector(ff_switchStatusChanged:)];
        [cell.contentView addSubview:s_systemNoti];
    } else {
        
        FFLab *lab_version = [[FFLab alloc] initWithText:[FFUtils ff_appVerson] txtColor:FFColorFromHex(0xd2d2d2) txtAli:NSTextAlignmentRight font:FFFontSize(15*ss)];
        [cell.contentView addSubview:lab_version];
        
        [lab_version mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(cell.contentView.mas_right).offset(-20*ss);
            make.centerY.mas_equalTo(cell.contentView.mas_centerY);
        }];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
}

#pragma mark - Switch Action Method

- (void)ff_switchStatusChanged:(id)sender
{
    FFSwitch *s = sender;
    if (s == s_systemNoti) {
        if (s_systemNoti.on) {
            //开
            NSLog(@"系统开");
        } else {
            //关
            NSLog(@"系统关");
        }
    } else {
        if (s_newMsgNotice.on) {
            //开
            NSLog(@"消息开");
        } else {
            //关
            NSLog(@"消息关");
        }
    }
}


@end
