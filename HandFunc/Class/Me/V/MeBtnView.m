//
//  MeBtnView.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/10.
//  Copyright © 2017年 ff. All rights reserved.
//  粉丝、收藏、动态

#import "MeBtnView.h"

@interface MeBtnView()
{

}

@end

@implementation MeBtnView

- (instancetype)initWithBtns:(NSArray *)btnArrs imgs:(NSArray *)imgNameArr
{
    self = [super init];
    if (self) {
        self.userInteractionEnabled = YES;
        if (btnArrs != nil && btnArrs.count > 0 && imgNameArr != nil && imgNameArr.count > 0) {
            for (NSInteger i = 0; i < btnArrs.count; i++) {
                CGFloat w = SCREENW/btnArrs.count;
                
                UIView *temp = [[UIView alloc] init];
                temp.frame = CGRectMake(i*w, 0, w, 70*ss);
                temp.backgroundColor = FFWhiteColor;
                temp.layer.borderWidth = 0.3;
                temp.layer.borderColor = FFBaseColor.CGColor;
                temp.userInteractionEnabled = YES;
                temp.tag = i;
                UIGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ff_btnTaped:)];
                [temp addGestureRecognizer:tap];
                [self addSubview:temp];
                
                UIImageView *img = [[UIImageView alloc] init];
                img.image = FFImg(imgNameArr[i]);
                img.frame = CGRectMake(0, 0, 50*ss, 50*ss);
                [temp addSubview:img];
                
                [img mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.mas_equalTo(temp.mas_centerX);
                    make.top.mas_equalTo(temp.mas_top).offset(10*ss);
                }];
                
                UILabel *lab = [[UILabel alloc] init];
                lab.text = btnArrs[i];
                lab.textColor = FFBaseColor;
                lab.textAlignment = NSTextAlignmentCenter;
                lab.frame = CGRectMake(0, 0, 100*ss, 30*ss);
                lab.font = FFFontSize(12*ss);
                [temp addSubview:lab];
                
                [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.mas_equalTo(temp.mas_centerX);
                    make.top.mas_equalTo(img.mas_bottom).offset(10*ss);
                }];
            }
        }
    }
    return self;
}

#pragma mark - Tap Action Method

- (void)ff_btnTaped:(id)sender
{
        UIView *imgBtn = sender;
    NSLog(@"TapTag");
    //    NSLog(@"TapTag:%d", imgBtn.tag);
        [self.delegate ff_tapWithIndex:imgBtn.tag];
}

@end
