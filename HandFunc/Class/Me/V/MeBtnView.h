//
//  MeBtnView.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/10.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MeBtnViewDelegate;

@interface MeBtnView : UIView

@property (nonatomic, assign) id<MeBtnViewDelegate>delegate;

- (instancetype)initWithBtns:(NSArray *)btnArrs imgs:(NSArray *)imgNameArr;

@end

@protocol MeBtnViewDelegate <NSObject>

- (void)ff_tapWithIndex:(NSInteger)index;

@end
