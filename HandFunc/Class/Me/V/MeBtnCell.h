//
//  MeBtnCell.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/10.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MeBtnView.h"

@interface MeBtnCell : UITableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@end
