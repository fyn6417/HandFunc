//
//  MePersonInfoView.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/10.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FFMePersonModel.h"

@interface MePersonInfoView : UIView

- (instancetype)initWithModel:(FFMePersonModel *)model;

@end
