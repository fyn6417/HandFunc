//
//  MePersonInfoView.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/10.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "MePersonInfoView.h"

@interface MePersonInfoView ()
{
    UIImageView *imgv_head;
    UILabel *lab_name;
    UILabel *lab_hospital;
    UILabel *lab_pTitle;
    UILabel *lab_pDep;
}

@end

@implementation MePersonInfoView

- (instancetype)initWithModel:(FFMePersonModel *)model
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, SCREENW, 70*ss);
        
        [self ff_buildViewWithModel:model];
        [self ff_layoutView];
    }
    return self;
}
#pragma mark - Build View Method

- (void)ff_buildViewWithModel:(FFMePersonModel *)model
{
    imgv_head = [[UIImageView alloc] initWithFrame:CGRectZero];
    imgv_head.image = FFImg(model.ff_img);
    [self addSubview:imgv_head];
    
    lab_name = [[UILabel alloc] initWithFrame:CGRectZero];
    lab_name.text = model.ff_name;
    lab_name.textColor = FFColorFromHex(0x414141);
    lab_name.textAlignment = NSTextAlignmentLeft;
    lab_name.font = FFFontSize(15*ss);
    [self addSubview:lab_name];
    
    lab_hospital = [[UILabel alloc] initWithFrame:CGRectZero];
    lab_hospital.text = model.ff_hospital;
    lab_hospital.textColor = FFColorFromHex(0x999999);
    lab_hospital.textAlignment = NSTextAlignmentLeft;
    lab_hospital.font = FFFontSize(10*ss);
    [self addSubview:lab_hospital];
    
    lab_pTitle = [[UILabel alloc] initWithFrame:CGRectZero];
    lab_pTitle.text = model.ff_pTitle;
    lab_pTitle.textColor = FFColorFromHex(0x999999);
    lab_pTitle.textAlignment = NSTextAlignmentLeft;
    lab_pTitle.font = FFFontSize(10*ss);
    [self addSubview:lab_pTitle];
    
    lab_pDep = [[UILabel alloc] initWithFrame:CGRectZero];
    lab_pDep.text = model.ff_pDep;
    lab_pDep.textColor = FFColorFromHex(0x999999);
    lab_pDep.textAlignment = NSTextAlignmentLeft;
    lab_pDep.font = FFFontSize(10*ss);
    [self addSubview:lab_pDep];
    
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    [imgv_head mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.mas_top).offset(10.5*ss);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.left.mas_equalTo(self.mas_left).offset(17.5*ss);
        make.size.mas_equalTo(CGSizeMake(50*ss, 50*ss));
    }];
    
    [lab_name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(imgv_head.mas_top);
        make.left.mas_equalTo(imgv_head.mas_right).offset(17.5*ss);
    }];
    
    [lab_hospital mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lab_name.mas_bottom).offset(5*ss);
        make.left.mas_equalTo(imgv_head.mas_right).offset(17.5*ss);
    }];
    
    [lab_pTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(imgv_head.mas_bottom);
        make.left.mas_equalTo(imgv_head.mas_right).offset(17.5*ss);
    }];
    
    [lab_pDep mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(lab_pTitle.mas_centerY);
        make.left.mas_equalTo(lab_pTitle.mas_right).offset(7.5*ss);
    }];
    
}

@end
