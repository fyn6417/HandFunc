//
//  MeAttentionView.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/10.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "MeAttentionView.h"

@interface MeAttentionView ()
{
//    UIImageView *img;
}

@end

@implementation MeAttentionView

- (instancetype)initWithImgArr:(NSArray *)arr
{
    self = [super init];
    if (self) {
        if (arr == nil || arr.count == 0) {
            return nil;
        }
        CGFloat w = 35*ss;
        for (NSInteger i = 0; i < arr.count; i++) {
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(i*ss*(10+w), 0, w, w)];
            img.image = FFImg(arr[i]);
            FFViewBorderRadius(img, w, 0.3, FFGrayColor);
            [self addSubview:img];
        }
    }
    return self;
}

@end
