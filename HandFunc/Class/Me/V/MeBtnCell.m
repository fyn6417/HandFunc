//
//  MeBtnCell.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/10.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "MeBtnCell.h"

@interface MeBtnCell ()
{
    MeBtnView *ff_btnView;
}

@end

@implementation MeBtnCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        ff_btnView = [[MeBtnView alloc] initWithBtns:@[@"我的粉丝", @"我的收藏", @"我的动态"] imgs:@[@"Icon40", @"Icon40", @"Icon40"]];
        [self.contentView addSubview:ff_btnView];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
