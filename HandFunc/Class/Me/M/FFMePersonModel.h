//
//  FFMePersonModel.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/10.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FFMePersonModel : NSObject

@property NSString *ff_name;
@property NSString *ff_img;
@property NSString *ff_hospital;
@property NSString *ff_pTitle;//职称
@property NSString *ff_pDep;//部门
@property NSString *ff_pAddr;//地址
@property NSString *ff_pDescription;//简介
@property NSString *ff_vipDate;//vip到期日期
@end
