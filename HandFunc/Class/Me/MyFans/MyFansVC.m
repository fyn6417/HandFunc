//
//  MyFansVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/13.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "MyFansVC.h"

#define fHeightForRow 65*ss

@interface MyFansVC ()<FFTitleBarDelegate, UITableViewDelegate, UITableViewDataSource>
{
    FFTitleBar *v_titleBar;
    UITableView *table_content;
//    FFBtn *btn;
    
    NSDictionary *dic;
}
@end

@implementation MyFansVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFBaseBGColor;
    
    dic = @{@"ff_img" : @"Icon40", @"ff_name" : @"Koala", @"ff_hospital" : @"大连医科大学附属第二医院", @"ff_pTitle" : @"主任医师", @"ff_pDep" : @"胸外科", @"ff_pAddr" : @"辽宁大连", @"ff_pDescription" : @"的机读卡和大伙回到家撒谎家世界换卡率加厚的家回到家花健康的可结案很淡定地阿虎UI各大UI更带感UI啊对u", @"ff_isAtten" : @"1"};
}
#pragma mark - Build View Method

- (void)ff_buildView
{
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"我的关注" leftImg:FFImg(@"Icon40") rightName:@""];
    v_titleBar.delegate = self;
    [self.view addSubview:v_titleBar];
    
    table_content = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    //    table_content.frame = CGRectMake(0, 74*ss, SCREENW, SCREENH-49*ss-74*ss);
    table_content.delegate = self;
    table_content.dataSource = self;
    table_content.backgroundColor = FFBaseBGColor;
    table_content.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:table_content];
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    [v_titleBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(64*ss);
    }];
    
    [table_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(v_titleBar.mas_bottom).offset(10*ss);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREENW, SCREENH-49*ss-74*ss));
    }];
}

#pragma mark - FFTitleBarDelegate Method

- (void)ff_leftBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate && UITableViewDataSource Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return fHeightForRow;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10*ss;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cID = @"cid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cID];
    }
    
//    NSInteger section = indexPath.section;
//    NSInteger row = indexPath.row;
    
    FFMePersonModel *model = [FFMePersonModel yy_modelWithDictionary:dic];//arr_list[row]
    MePersonInfoView *view = [[MePersonInfoView alloc] initWithModel:model];
    [cell.contentView addSubview:view];
    NSInteger tag = [dic[@"ff_isAtten"] integerValue];
    
    FFBtn *btn = [[FFBtn alloc] initWithTitle:tag == 0 ? @"关注" : @"取消关注" titleColor:FFWhiteColor titleFont:FFFontSize(11*ss) bgColor:FFBaseColor target:self action:@selector(ff_btnClicked:)];
    [cell.contentView addSubview:btn];
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(cell.contentView.mas_right).offset(-16*ss);
        make.centerY.mas_equalTo(cell.contentView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(60*ss, 20*ss));
    }];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
}

#pragma mark - Btn Action Method

- (void)ff_btnClicked:(id)sender
{
    
}

@end
