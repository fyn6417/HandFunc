//
//  PersonInfoVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/11.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "PersonInfoVC.h"

@interface PersonInfoVC ()<FFTitleBarDelegate, UITableViewDelegate, UITableViewDataSource>
{
    FFMePersonModel *ff_model;
    UITableView *table_person;
    FFTitleBar *v_titleBar;
    
    NSArray *arr_infoTitle;
}
@end

@implementation PersonInfoVC

- (instancetype)initWithModel:(FFMePersonModel *)model
{
    self = [super init];
    if (self) {
        if (model) {
            ff_model = model;
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFColorFromHex(0xaaaaaa);
    
    arr_infoTitle = @[@"姓名", @"单位", @"部门", @"职务", @"地区"];
}
#pragma mark - Build View Method

- (void)ff_buildView
{
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"个人资料" leftImg:FFImg(@"icon_back") rightImg:FFImg(@"")];
    v_titleBar.delegate = self;
    [self.view addSubview:v_titleBar];
    
    table_person = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    table_person.delegate = self;
    table_person.dataSource = self;
    table_person.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    table_person.backgroundColor = FFBaseBGColor;
    [self.view addSubview:table_person];
    
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    
    [v_titleBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(64*ss);
    }];
    
    [table_person mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(v_titleBar.mas_bottom);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREENW, SCREENH-49*ss-74*ss));
    }];
}


#pragma mark - UITableViewDelegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 5;
    } else {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return 70*ss;
        }
        return fKeyRowH;
    } else {
        return 80*ss;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10*ss;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cID = @"cid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cID];
    }
    
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    NSArray *arr = @[ff_model.ff_hospital, ff_model.ff_pDep, ff_model.ff_pTitle, ff_model.ff_pAddr, ff_model.ff_pDescription];
    if (section == 0) {
        UILabel *lab = [self ff_labWithText:arr_infoTitle[row] font:FFFontSize(15*ss) txtColor:FFColorFromHex(0x414141) txtAli:NSTextAlignmentLeft];
        [cell.contentView addSubview:lab];
        
        [lab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(cell.contentView.mas_left).offset(20*ss);
            make.centerY.mas_equalTo(cell.contentView.mas_centerY);
        }];
        if (row == 0) {
            //CGRectMake(280, 1.5*ss, 57*ss, 57*ss)
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectZero];
            img.image = FFImg(ff_model.ff_img);
            [cell.contentView addSubview:img];
            
            [img mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(cell.contentView.mas_right);
                make.centerY.mas_equalTo(cell.contentView.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(57*ss, 57*ss));
            }];
        } else {
            UILabel *rLab = [self ff_labWithText:arr[row-1] font:FFFontSize(15*ss) txtColor:FFColorFromHex(0xaaaaaa) txtAli:NSTextAlignmentRight];
            [cell.contentView addSubview:rLab];
            
            [rLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(cell.contentView.mas_right);
                make.centerY.mas_equalTo(cell.contentView.mas_centerY);
//                make.size.mas_equalTo(CGSizeMake(<#CGFloat width#>, <#CGFloat height#>));
            }];
        }
    } else {
        UILabel *lab = [self ff_labWithText:@"个人简介" font:FFFontSize(15*ss) txtColor:FFColorFromHex(0x414141) txtAli:NSTextAlignmentLeft];
        lab.frame = CGRectMake(20*ss, 10*ss, 100, 35);
        [cell.contentView addSubview:lab];
        
        UITextView *tx = [[UITextView alloc] initWithFrame:CGRectMake(20*ss, CGRectGetMaxY(lab.frame)+4, SCREENW-80*ss, 100)];
        tx.textColor = FFColorFromHex(0xaaaaaa);
        tx.text = [NSString stringWithFormat:@"    %@", ff_model.ff_pDescription];
        [cell.contentView addSubview:tx];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
}

#pragma mark - FFTitleBarDelegate Method

- (void)ff_leftBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - view init Method

- (UILabel *)ff_labWithText:(NSString *)text font:(UIFont *)font txtColor:(UIColor *)color txtAli:(NSTextAlignment)ali
{
    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectZero];
    lab.text = text;
    lab.textColor = color;//FFColorFromHex(0x414141);
    lab.textAlignment = ali;
    lab.font = font;
    return lab;
}

@end
