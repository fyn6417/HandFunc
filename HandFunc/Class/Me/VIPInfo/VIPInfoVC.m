//
//  VIPInfoVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/11.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "VIPInfoVC.h"

@interface VIPInfoVC ()<FFTitleBarDelegate, UITableViewDelegate, UITableViewDataSource, FFVipOptInfoViewDelegate>
{
    FFMePersonModel *ff_model;
    FFTitleBar *v_titleBar;
    UITableView *table_content;
    NSArray *arr_config;
    NSDictionary *dic;
    NSArray *arrTemp;
}

@end

@implementation VIPInfoVC

- (instancetype)initWithModel:(FFMePersonModel *)model
{
    self = [super init];
    if (self) {
        ff_model = model;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Config Method

- (void)ff_config
{
    
    self.view.backgroundColor = FFColorFromHex(0x414141);
    dic = @{@"ff_img" : @"Icon40", @"ff_name" : @"Koala", @"ff_hospital" : @"大连医科大学附属第二医院", @"ff_pTitle" : @"主任医师", @"ff_pDep" : @"胸外科", @"ff_pAddr" : @"辽宁大连", @"ff_vipDate" : @"2018-07-12", @"ff_pDescription" : @"的机读卡和大伙回到家撒谎家世界换卡率加厚的家回到家花健康的可结案很淡定地阿虎UI各大UI更带感UI啊对u"};
    arrTemp = @[@{@"img" : @"vip12", @"month" : @"12个月", @"price" : @"¥ 368.00", @"bill" : @"1"},
                @{@"img" : @"vip6", @"month" : @"6个月", @"price" : @"¥ 188.00", @"bill" : @"0"},
                @{@"img" : @"vip1", @"month" : @"1个月", @"price" : @"¥ 38.00", @"bill" : @"0"}];
}

#pragma mark - Build View Method

- (void)ff_buildView
{
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"VIP个人中心" leftImg:FFImg(@"icon_back") rightName:@"充值记录"];
    v_titleBar.delegate = self;
    [self.view addSubview:v_titleBar];
    
    table_content = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    table_content.delegate = self;
    table_content.dataSource = self;
    table_content.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    table_content.backgroundColor = FFBaseBGColor;
    [self.view addSubview:table_content];
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    [v_titleBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(64*ss);
    }];
    
    [table_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(v_titleBar.mas_bottom);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREENW, SCREENH-49*ss-74*ss));
    }];
}

#pragma mark - FFTitleBarDelegate Method

- (void)ff_leftBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 3) {
        return 3;
    } else {
        return 1;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    if (section == 0) {
        return 80*ss;
    } else {
        return fKeyRowH;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10*ss;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cID = @"cid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cID];
    }
    
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    ff_model = [FFMePersonModel yy_modelWithDictionary:dic];
    if (section == 0) {
        MePersonInfoView *personInfo = [[MePersonInfoView alloc] initWithModel:ff_model];
        [cell.contentView addSubview:personInfo];
        
        [personInfo mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(cell.contentView.mas_left);
            make.centerY.mas_equalTo(cell.contentView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(SCREENW, 80*ss));
        }];
    } else if (section == 1) {
        cell.textLabel.text = @"会员到期时间";
        cell.textLabel.font = FFFontSize(14*ss);
        cell.textLabel.textColor = FFColorFromHex(0x414141);
        FFLab *lab_date = [[FFLab alloc] initWithText:ff_model.ff_vipDate txtColor:FFColorFromHex(0x999999) txtAli:NSTextAlignmentRight font:FFFontSize(15*ss)];
        [cell.contentView addSubview:lab_date];
        [lab_date mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(cell.contentView.mas_centerY);
            make.right.mas_equalTo(cell.contentView.mas_right).offset(-16*ss);
        }];
    } else if (section == 2) {
        cell.textLabel.text = @"会员服务协议";
        cell.textLabel.textColor = FFColorFromHex(0x414141);
        cell.textLabel.font = FFFontSize(14*ss);
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else {
        NSInteger bill = [[arrTemp[row] objectForKey:@"bill"] integerValue];
        FFVipOptInfoView *vip = [[FFVipOptInfoView alloc] initWithImg:FFImg([arrTemp[row] objectForKey:@"img"]) monthNum:[arrTemp[row] objectForKey:@"month"] price:[arrTemp[row] objectForKey:@"price"] isBill:bill==1? YES :NO];
        vip.delegate = self;
        [cell.contentView addSubview:vip];
        [vip mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(cell.contentView.mas_centerY);
        }];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 2) {
        //会员服务协议
    }
}


#pragma mark - FFVipOptInfoViewDelegate Method

- (void)ff_dredgeClicked
{
    NSLog(@"立即开通");
}

@end
