//
//  FFVipOptInfoView.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/12.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "FFVipOptInfoView.h"

@interface FFVipOptInfoView()
{
    FFBtn *btn_dredge;
}

@end

@implementation FFVipOptInfoView

- (instancetype)initWithImg:(UIImage *)img monthNum:(NSString *)num price:(NSString *)rPrice isBill:(BOOL)bill
{
    self = [super init];
    if (self) {
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectZero];
        icon.image = img;
        [self addSubview:icon];
        
        FFLab *lab_month = [[FFLab alloc] initWithText:num txtColor:FFColorFromHex(0x414141) txtAli:NSTextAlignmentCenter font:FFFontSize(14*ss)];
        [self addSubview:lab_month];
        
        FFLab *lab_price = [[FFLab alloc] initWithText:rPrice txtColor:FFColorFromHex(0x414141) txtAli:NSTextAlignmentCenter font:FFFontSize(14*ss)];
        [self addSubview:lab_price];
        
        if (bill) {
            FFLab *lab_bill = [[FFLab alloc] initWithText:@"(可开发票)" txtColor:FFBaseColor txtAli:NSTextAlignmentCenter font:FFFontSize(12*ss)];
            [self addSubview:lab_bill];
            
            [lab_bill mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(lab_price.mas_right).offset(8*ss);
                make.bottom.mas_equalTo(lab_price.mas_bottom);
                //            make.size.mas_equalTo(CGSizeMake(<#CGFloat width#>, <#CGFloat height#>));
            }];
        }
        
        btn_dredge = [[FFBtn alloc] initWithTitle:@"立即开通" titleColor:FFWhiteColor titleFont:FFFontSize(11*ss) bgColor:FFBaseColor target:self action:@selector(ff_btnClicked)];
        btn_dredge.layer.cornerRadius = 2;
//        btn_dredge.frame = CGRectMake(300*ss, 12.5*ss, 60*ss, 20*ss);
        [self addSubview:btn_dredge];
        
        [icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_top).offset(16*ss);
            make.left.mas_equalTo(self.mas_left).offset(16*ss);
            make.centerY.mas_equalTo(self.mas_centerY);
//            make.size.mas_equalTo(CGSizeMake(<#CGFloat width#>, <#CGFloat height#>));
        }];
        
        [lab_month mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_right).offset(55*ss);
            make.centerY.mas_equalTo(self.mas_centerY);
//            make.size.mas_equalTo(CGSizeMake(<#CGFloat width#>, <#CGFloat height#>));
        }];
        
        [lab_price mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_right).offset(110*ss);
            make.centerY.mas_equalTo(self.mas_centerY);
            //            make.size.mas_equalTo(CGSizeMake(<#CGFloat width#>, <#CGFloat height#>));
        }];
        
        [btn_dredge mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.mas_right).offset((SCREENW - 16)*ss);
            make.centerY.mas_equalTo(self.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(60*ss, 20*ss));
        }];
        
    }
    return self;
}


#pragma mark - Btn Action Method

- (void)ff_btnClicked
{
    [self.delegate ff_dredgeClicked];
}

@end
