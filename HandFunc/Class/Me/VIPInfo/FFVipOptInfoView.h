//
//  FFVipOptInfoView.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/12.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FFVipOptInfoViewDelegate;

@interface FFVipOptInfoView : UIView

@property (nonatomic, assign) id<FFVipOptInfoViewDelegate>delegate;

- (instancetype)initWithImg:(UIImage *)img monthNum:(NSString *)num price:(NSString *)rPrice isBill:(BOOL)bill;

@end

@protocol FFVipOptInfoViewDelegate <NSObject>

- (void)ff_dredgeClicked;

@end
