//
//  AboutUsVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/11.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "AboutUsVC.h"

@interface AboutUsVC ()<FFTitleBarDelegate, UITableViewDelegate, UITableViewDataSource>
{
    UITableView *table_content;
    FFTitleBar *v_titleBar;
}
@end

@implementation AboutUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFColorFromHex(0xaaaaaa);
}

#pragma mark - Build View Method

- (void)ff_buildView
{
    v_titleBar = [[FFTitleBar alloc] initWithTitle:@"关于平台" leftImg:FFImg(@"icon_back") rightImg:FFImg(@"")];
    v_titleBar.delegate = self;
    [self.view addSubview:v_titleBar];
    
    table_content = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    table_content.delegate = self;
    table_content.dataSource = self;
    table_content.backgroundColor = FFBaseBGColor;
    table_content.tableFooterView = [UIView new];
    
    [self.view addSubview:table_content];
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    [v_titleBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(64*ss);
    }];
    
    [table_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(v_titleBar.mas_bottom);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREENW, SCREENH-49*ss-74*ss));
    }];
}

#pragma mark - UITableViewDelegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 400*ss;
    } else {
        return 35*ss;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10*ss;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cID = @"cid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cID];
    }
    
    NSInteger section = indexPath.section;
//    NSInteger row = indexPath.row;
    
    if (section == 0) {
        //
    } else {
        cell.textLabel.text = @"商务合作";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
}

#pragma mark - FFTitleBarDelegate Method

- (void)ff_leftBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
