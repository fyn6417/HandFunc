//
//  FFMeVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/6/28.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "FFMeVC.h"

@interface FFMeVC ()<FFBtnArrDelegate ,UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>
{
    FFTitleBar *view_titleBar;
    UITableView *table_content;
    NSArray *arr_imgset;
    NSArray *arr_labset;
    
//    FFBtnArr *btnView;
    
    NSDictionary *dic;//假数据
}
@end

@implementation FFMeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildView];
    [self ff_layoutView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFWhiteColor;
    self.navigationController.navigationBar.hidden = YES;
//    self.title = @"个人中心";
    
    arr_imgset = @[@"myAttention", @"msgNoti", @"sysNoti", @"vipInfo", @"aboutUs"];
    arr_labset = @[@"我的关注", @"消息通知", @"系统设置", @"会员信息", @"关于我们"];
    
    dic = @{@"ff_img" : @"Icon40", @"ff_name" : @"Koala", @"ff_hospital" : @"大连医科大学附属第二医院", @"ff_pTitle" : @"主任医师", @"ff_pDep" : @"胸外科", @"ff_pAddr" : @"辽宁大连", @"ff_pDescription" : @"的机读卡和大伙回到家撒谎家世界换卡率加厚的家回到家花健康的可结案很淡定地阿虎UI各大UI更带感UI啊对u"};
}

#pragma mark - Build View Method

- (void)ff_buildView
{
    view_titleBar = [[FFTitleBar alloc] initWithTitle:@"个人中心" leftImg:FFImg(@"") rightName:@""];
    [self.view addSubview:view_titleBar];
    
//    btnView = [[FFBtnArr alloc] initWithBtns:@[@"我的粉丝", @"我的收藏", @"我的动态"] imgs:@[@"myFans", @"myFavorites", @"myPost"]];
//    btnView.delegate = self;
//    [self.view addSubview:btnView];
    
    
    table_content = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
//    table_content.frame = CGRectMake(0, 74*ss, SCREENW, SCREENH-49*ss-74*ss);
    table_content.delegate = self;
    table_content.dataSource = self;
    table_content.backgroundColor = FFBaseBGColor;
    table_content.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:table_content];
}

#pragma mark - Layout View Method

- (void)ff_layoutView
{
    [view_titleBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(64*ss);
    }];
    
//    [btnView mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.top.mas_equalTo(view_titleBar.mas_bottom).offset(10*ss);
//                make.left.mas_equalTo(self.view.mas_left);
//    }];
    [table_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(view_titleBar.mas_bottom).offset(10*ss);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREENW, SCREENH-49*ss-74*ss));
    }];
}

#pragma mark - UITableViewDelegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([FFUtils ff_isLogin]) {
        //登录
        return 5;
    } else {
        return 4;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 3) {
        return 4;
    } else {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 || indexPath.section == 1) {
        return 70*ss;
    } else if (indexPath.section == 2) {
        return 60*ss;
    } else {
        return fKeyRowH;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 4) {
        return 35*ss;
    } else {
        return 5*ss;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cID = @"cid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cID];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.userInteractionEnabled = YES;
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    if (section == 0) {
        
        MePersonInfoView *pInfoView = [[MePersonInfoView alloc] initWithModel:[FFMePersonModel yy_modelWithDictionary:dic]];
        [cell.contentView addSubview:pInfoView];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    
    } else if (section == 1) {
        FFBtnArr *btnView = [[FFBtnArr alloc] initWithBtns:@[@"我的粉丝", @"我的收藏", @"我的动态"] imgs:@[@"myFans", @"myFavorites", @"myPost"] w:SCREENW];
        btnView.delegate = self;
        
//        UIButton *btnView = [UIButton buttonWithType:UIButtonTypeCustom];
//        btnView.frame = CGRectMake(10, 20, 100, 30);
//        btnView.backgroundColor = FFBaseColor;
//        [btnView  setTitle:@"Fuck" forState:UIControlStateNormal];
//        [btnView addTarget:self action:@selector(ff_tapWithIndex:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:btnView];
        return cell;
    } else if (section == 2) {
        cell.imageView.image = FFImg(arr_imgset[row]);
        cell.textLabel.text = arr_labset[row];
        cell.textLabel.font = FFFontSize(14*ss);
        cell.textLabel.textColor = FFColorFromHex(0x414141);
        
        MeAttentionView *attentionView = [[MeAttentionView alloc] initWithImgArr:@[@"msgNoti", @"sysNoti", @"vipInfo", @"aboutUs",]];
        [cell.contentView addSubview:attentionView];
        
        [attentionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(cell.contentView.mas_left).offset(190*ss);
            make.top.mas_equalTo(cell.contentView.mas_top).offset(12.5*ss);
        }];
        
        return cell;
    } else if (section == 3) {
        cell.imageView.image = FFImg(arr_imgset[row+1]);
        cell.textLabel.text = arr_labset[row+1];
        cell.textLabel.font = FFFontSize(14*ss);
        cell.textLabel.textColor = FFColorFromHex(0x414141);
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    } else {
        if ([FFUtils ff_isLogin]) {
            FFBtn *btn_logout = [[FFBtn alloc] initWithTitle:@"退出登录" titleColor:FFRedColor titleFont:FFFontSize(16*ss) bgColor:FFWhiteColor target:self action:@selector(ff_logOut)];
            
//            [cell.contentView addSubview:btn_logout];
            return cell;
        } else {
            return nil;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    if (section == 0) {
        [self ff_gotoPersonInfoCenter];
    } else if (section == 2) {
        [self ff_gotoMyAttention];
    } else if (section == 3) {
        switch (indexPath.row) {
            case 0:
                [self ff_gotoMsgNoti];
                break;
            case 1:
                [self ff_gotoSystemSetting];
                break;
            case 2:
                [self ff_gotoVIPInfo];
                break;
            case 3:
                [self ff_gotoAboutUs];
                break;
                
            default:
                break;
        }
    }
    
}

#pragma mark - Push To Method

- (void)ff_gotoPersonInfoCenter
{
    PersonInfoVC *info = [[PersonInfoVC alloc] initWithModel:[FFMePersonModel yy_modelWithDictionary:dic]];
    [self.navigationController pushViewController:info animated:YES];
}

- (void)ff_gotoMyFans
{
    MyFansVC *vc = [MyFansVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)ff_gotoMyFavorites
{
    MyFavoritesVC *vc = [MyFavoritesVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)ff_gotoMyPost
{
    MyPostVC *vc = [MyPostVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)ff_gotoMyAttention
{
    MyAttentionVC *vc = [MyAttentionVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)ff_gotoMsgNoti
{
    MsgNotiVC *vc = [MsgNotiVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)ff_gotoSystemSetting
{
    SysSettingVC *vc = [SysSettingVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)ff_gotoVIPInfo
{
    VIPInfoVC *vc = [VIPInfoVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)ff_gotoAboutUs
{
    AboutUsVC *vc = [AboutUsVC new];
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - FFBtnArrDelegate Method

- (void)ff_tapWithIndex:(NSInteger)index
{
    if (index == 0) {
        [self ff_gotoMyFans];
    } else if (index == 1) {
        [self ff_gotoMyFavorites];
    } else {
        [self ff_gotoMyPost];
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    // 输出点击的view的类名
    NSLog(@"%@", NSStringFromClass([touch.view class]));
    
    // 若为UITableViewCellContentView（即点击了tableViewCell），则不截获Touch事件
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {
        return NO;
    }
    return  YES;
}

#pragma mark - Btn Action Method

- (void)ff_logOut
{
    
}


@end
