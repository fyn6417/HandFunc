//
//  SignUpVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/6/28.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "SignUpVC.h"


@interface SignUpVC ()<UITextFieldDelegate, FFThirdPartyViewDelegate>
{
    UIButton *btn_dismiss;
    
    UILabel *lab_title;
    UILabel *lab_description;
    
    FFAuthTFView *tf_phoneNum;
    FFAuthTFView *tf_authCode;
    FFAuthTFView *tf_pwdOne;
    FFAuthTFView *tf_pwdTwo;
    
    UIButton *btn_signUp;
    UIButton *btn_login;
    
    FFThirdPartyView *v_thirdParty;
}
@end

@implementation SignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildViews];
    [self ff_layoutSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFWhiteColor;
    self.view.userInteractionEnabled = YES;
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ff_endEditing:)]];
}

#pragma mark - Build View Method

- (void)ff_buildViews
{
    btn_dismiss = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_dismiss setImage:FFImg(@"Icon40") forState:UIControlStateNormal];
    [btn_dismiss addTarget:self action:@selector(ff_dismissAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn_dismiss];
    
    lab_title = [[UILabel alloc] initWithFrame:CGRectZero];
    lab_title.text = @"手机快捷注册";
    lab_title.textColor = FFColorFromHex(0x414141);
    lab_title.textAlignment = NSTextAlignmentLeft;
    lab_title.font = FFFontSize(18*ss);
    [self.view addSubview:lab_title];
    
    lab_description = [[UILabel alloc] initWithFrame:CGRectZero];
    lab_description.numberOfLines = 0;
    lab_description.text = @"用户手机端注册，我们将会保障您的私人信息安全\n祝你使用愉快";
    lab_description.textColor = FFColorFromHex(0xaaaaaa);
    lab_description.textAlignment = NSTextAlignmentLeft;
    lab_description.font = FFFontSize(13*ss);
    [self.view addSubview:lab_description];
    
    NSArray *arr = @[@"请输入手机号", @"请输入验证码", @"请输入密码", @"请再次输入密码"];
    tf_phoneNum = [[FFAuthTFView alloc] initWithPlaceHolder:arr[0] labText:@"获取验证码"];
    tf_phoneNum.tf_content.delegate = self;
    [self.view addSubview:tf_phoneNum];
    
    tf_authCode = [[FFAuthTFView alloc] initWithPlaceHolder:arr[1]];
    tf_authCode.tf_content.delegate = self;
    [self.view addSubview:tf_authCode];
    
    tf_pwdOne = [[FFAuthTFView alloc] initWithPlaceHolder:arr[2]];
    tf_pwdOne.tf_content.delegate = self;
    [self.view addSubview:tf_pwdOne];
    
    tf_pwdTwo = [[FFAuthTFView alloc] initWithPlaceHolder:arr[3]];
    tf_pwdTwo.tf_content.delegate = self;
    [self.view addSubview:tf_pwdTwo];
    
    btn_signUp = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_signUp.frame = CGRectZero;
    btn_signUp.layer.cornerRadius = FFCorner;
    btn_signUp.backgroundColor = FFBaseColor;
    [btn_signUp setTitle:@"注  册" forState:UIControlStateNormal];
    [btn_signUp setTitleColor:FFColorFromHex(0xfafbfd) forState:UIControlStateNormal];
    btn_signUp.titleLabel.font = FFFontSize(16*ss);
    [btn_signUp addTarget:self action:@selector(ff_btnSignInAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn_signUp];
    
    
    btn_login = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_login.frame = CGRectZero;
    
    [btn_login setTitle:@"已 有 账 号 ，立 即 登 录" forState:UIControlStateNormal];
    [btn_login setTitleColor:FFBaseColor forState:UIControlStateNormal];
    btn_login.titleLabel.font = FFFontSize(10*ss);
    [btn_login addTarget:self action:@selector(ff_dismissAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn_login];
    
    v_thirdParty = [[FFThirdPartyView alloc] init];
    v_thirdParty.frame = CGRectZero;
    v_thirdParty.delegate = self;
    [self.view addSubview:v_thirdParty];
}

#pragma mark - Layout Method

- (void)ff_layoutSubviews
{
    [btn_dismiss mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).offset(50*ss);
        make.left.mas_equalTo(self.view.mas_left).offset(20*ss);
    }];
    
    [lab_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).offset(124*ss);
        make.left.mas_equalTo(self.view.mas_left).offset(36*ss);
    }];
    
    [lab_description mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lab_title.mas_bottom).offset(15*ss);
        make.left.mas_equalTo(lab_title.mas_left);
        make.width.mas_equalTo(300*ss);
    }];
    
    [tf_phoneNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lab_description.mas_bottom).offset(50*ss);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(282*ss, 35*ss));
    }];
    
    [tf_authCode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(tf_phoneNum.mas_bottom).offset(15*ss);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(282*ss, 35*ss));
    }];
    
    [tf_pwdOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(tf_authCode.mas_bottom).offset(15*ss);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(282*ss, 35*ss));
    }];
    
    [tf_pwdTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(tf_pwdOne.mas_bottom).offset(15*ss);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(282*ss, 35*ss));
    }];
    
    [btn_signUp mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(tf_pwdTwo.mas_bottom).offset(25*ss);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(282*ss, 42*ss));
    }];
    
    [btn_login mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(btn_signUp.mas_bottom).offset(15*ss);
        make.centerX.mas_equalTo(self.view.mas_centerX);
    }];
    
    [v_thirdParty mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(SCREENW, 100*ss));
    }];
    
}

#pragma mark - Btn Action Method

- (void)ff_dismissAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)ff_btnSignInAction
{
    
}

#pragma mark - Tap Action Method

- (void)ff_endEditing:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark - FFThirdPartyViewDelegate Method

- (void)ff_weixinLogin
{

}

- (void)ff_qqLogin
{

}

@end
