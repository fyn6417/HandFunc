//
//  LoginVC.m
//  HandFunc
//
//  Created by ff'Mac on 2017/6/28.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "LoginVC.h"

@interface LoginVC ()<UITextFieldDelegate, FFThirdPartyViewDelegate>
{
    UIImageView *imgv_logo;
    
    UILabel *lab_title;
    UILabel *lab_description;
    
    FFLoginTextField *tf_userName;
    FFLoginTextField *tf_pwd;
    
    UIButton *btn_login;
    UIButton *btn_signUp;
    
    UIButton *btn_forgetPWD;
    
    FFThirdPartyView *v_thirdParty;
}
@end

@implementation LoginVC

#pragma mark - Life Circle Method

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ff_config];
    [self ff_buildViews];
    [self ff_layoutSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Config Method

- (void)ff_config
{
    self.view.backgroundColor = FFWhiteColor;
    self.view.userInteractionEnabled = YES;
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ff_endEditing:)]];
}

#pragma mark - Build View Method

- (void)ff_buildViews
{
    imgv_logo = [[UIImageView alloc] initWithFrame:CGRectZero];
    imgv_logo.image = FFImg(@"logoLogin");
    [self.view addSubview:imgv_logo];
    
    lab_title = [[UILabel alloc] initWithFrame:CGRectZero];
    lab_title.text = @"中国手功能康复";
    lab_title.textColor = FFBaseColor;
    lab_title.textAlignment = NSTextAlignmentCenter;
    lab_title.font = [UIFont boldSystemFontOfSize:22*ss];
    [self.view addSubview:lab_title];
    
    lab_description = [[UILabel alloc] initWithFrame:CGRectZero];
    lab_description.text = @"规范化信息平台";
    lab_description.textColor = FFBaseColor;
    lab_description.textAlignment = NSTextAlignmentCenter;
    lab_description.font = FFFontSize(18*ss);
    [self.view addSubview:lab_description];
    
    tf_userName = [[FFLoginTextField alloc] initWithIcon:FFImg(@"Icon20") placeHolder:@"Username"];
    tf_userName.tf_content.delegate = self;
    tf_userName.frame = CGRectZero;
    [self.view addSubview:tf_userName];
    
    tf_pwd = [[FFLoginTextField alloc] initWithIcon:FFImg(@"Icon20") placeHolder:@"Password"];
    tf_pwd.tf_content.delegate = self;
    tf_pwd.frame = CGRectZero;
    [self.view addSubview:tf_pwd];
    
    btn_login = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_login.frame = CGRectZero;
    btn_login.layer.cornerRadius = FFCorner;
    btn_login.backgroundColor = FFBaseColor;
    [btn_login setTitle:@"登  录" forState:UIControlStateNormal];
    [btn_login setTitleColor:FFColorFromHex(0xfafbfd) forState:UIControlStateNormal];
    btn_login.titleLabel.font = FFFontSize(16*ss);
    [btn_login addTarget:self action:@selector(ff_btnLoginAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn_login];
    
    btn_signUp = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_signUp.frame = CGRectZero;
    btn_signUp.layer.cornerRadius = FFCorner;
    btn_signUp.layer.borderColor = FFBaseColor.CGColor;
    btn_signUp.layer.borderWidth = 1.0f;
    btn_signUp.backgroundColor = FFColorFromHex(0xfafbfd);
    [btn_signUp setTitle:@"注  册" forState:UIControlStateNormal];
    [btn_signUp setTitleColor:FFBaseColor forState:UIControlStateNormal];
    btn_signUp.titleLabel.font = FFFontSize(16*ss);
    [btn_signUp addTarget:self action:@selector(ff_btnSignUpAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn_signUp];
    
    btn_forgetPWD = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_forgetPWD.frame = CGRectZero;
    
    [btn_forgetPWD setTitle:@"忘记密码？" forState:UIControlStateNormal];
    [btn_forgetPWD setTitleColor:FFBaseColor forState:UIControlStateNormal];
    btn_forgetPWD.titleLabel.font = FFFontSize(10*ss);
    [btn_forgetPWD addTarget:self action:@selector(ff_btnForgetPWDAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn_forgetPWD];
    
    v_thirdParty = [[FFThirdPartyView alloc] init];
    v_thirdParty.frame = CGRectZero;
    v_thirdParty.delegate = self;
    [self.view addSubview:v_thirdParty];
}

#pragma mark - Layout Method

- (void)ff_layoutSubviews
{
    [imgv_logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).offset(83*ss);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(71*ss, 71*ss));
    }];
    
    [lab_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(imgv_logo.mas_bottom).offset(21*ss);
        make.centerX.mas_equalTo(imgv_logo.mas_centerX);
    }];
    
    [lab_description mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lab_title.mas_bottom).offset(10*ss);
        make.centerX.mas_equalTo(imgv_logo.mas_centerX);
    }];
    
    [tf_userName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.centerY.mas_equalTo(self.view.mas_centerY).offset(-13*ss);
        make.size.mas_equalTo(CGSizeMake(282*ss, 25*ss));
    }];
    
    [tf_pwd mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(tf_userName.mas_centerX);
        make.top.mas_equalTo(tf_userName.mas_bottom).offset(25*ss);
        make.size.mas_equalTo(CGSizeMake(282*ss, 25*ss));
    }];
    
    [btn_login mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(tf_pwd.mas_bottom).offset(25*ss);
        make.centerX.mas_equalTo(tf_userName.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(282*ss, 42*ss));
    }];
    
    [btn_signUp mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(btn_login.mas_bottom).offset(15*ss);
        make.centerX.mas_equalTo(tf_userName.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(282*ss, 42*ss));
    }];
    
    [btn_forgetPWD mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(btn_signUp.mas_bottom).offset(14*ss);
        make.right.mas_equalTo(btn_signUp.mas_right);
        make.size.mas_equalTo(CGSizeMake(55*ss, 15*ss));
    }];
    
    [v_thirdParty mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(SCREENW, 120*ss));
    }];
}

#pragma mark - Btn Action Method

- (void)ff_btnLoginAction
{
    NSString *userName = tf_userName.tf_content.text;
    NSString *pwd = tf_pwd.tf_content.text;
    if (userName != nil && userName.length != 0) {
        if (pwd != nil && pwd.length != 0) {
            NSLog(@"💯登录ID:%@", tf_userName.tf_content.text);
            NSLog(@"💯密码ID:%@", tf_pwd.tf_content.text);
            [self dismissViewControllerAnimated:YES completion:^{
                
                [[NSUserDefaults standardUserDefaults] setObject:userName forKey:fKeyUserName];
                [[NSUserDefaults standardUserDefaults] setObject:pwd forKey:fKeyUserPWD];
                [[NSUserDefaults standardUserDefaults] setObject:fKeyLogin forKey:fKeyLogin];
            }];
        } else {
            //密码不能为空
        }
    } else {
        //账号不能为空
    }
}
- (void)ff_btnSignUpAction
{
    NSLog(@"💯注册");
}
- (void)ff_btnForgetPWDAction
{
    NSLog(@"💯忘记密码");
}

#pragma mark - Tap Action Method

- (void)ff_endEditing:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark - FFThirdPartyViewDelegate Method

- (void)ff_weixinLogin
{
    NSLog(@"💯微信登录");
}
- (void)ff_qqLogin
{
    NSLog(@"💯QQ登录");
}

#pragma mark - Net Method

- (void)ff_getCode
{
    [[FFNetwork sharedManager] ff_requsetWithMethod:GET path:@"https://www.bestguy.net/v1/public/news/list/1/1/1" params:nil success:^(NSDictionary *dic) {
        NSLog(@"%@", dic);
    } failure:^(NSError *err) {
        //
    }];
}


@end
