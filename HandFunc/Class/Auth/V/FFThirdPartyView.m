//
//  FFThirdPartyView.m
//  HandFunc
//
//  Created by ff'Mac on 2017/6/28.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "FFThirdPartyView.h"

#define IconWeixinSize CGSizeMake(30*ss, 27*ss)
#define IconQQSize     CGSizeMake(25*ss, 27*ss)
#define marginForIcon                  30*ss
#define marginForLab                    8*ss

@interface FFThirdPartyView()
{
    UIView *v_line;
    UILabel *lab;//第三方登录lab
    
    UIImageView *imgv_weixin;
    UILabel *lab_weixin;
    UIImageView *imgv_qq;
    UILabel *lab_qq;
}
@end

@implementation FFThirdPartyView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.userInteractionEnabled = YES;
        self.backgroundColor = FFWhiteColor;
        
        v_line = [[UIView alloc] initWithFrame:CGRectZero];
        v_line.backgroundColor = FFColorFromHex(0xc9c9c9);
        v_line.alpha = .8;
        [self addSubview:v_line];
        
        lab = [[UILabel alloc] initWithFrame:CGRectZero];
        lab.backgroundColor = FFWhiteColor;
        lab.font = FFFontSize(10*ss);
        lab.textColor = FFColorFromHex(0xc9c9c9);
        lab.textAlignment = NSTextAlignmentCenter;
        lab.text = @"第 三 方 快 速 登 录";
        [self addSubview:lab];
        
        imgv_weixin = [[UIImageView alloc] initWithFrame:CGRectZero];
        imgv_weixin.userInteractionEnabled = YES;
        imgv_weixin.image = FFImg(@"weixinLogin");
        [imgv_weixin addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ff_tapWeixin)]];
        [self addSubview:imgv_weixin];
        
        lab_weixin = [[UILabel alloc] initWithFrame:CGRectZero];
        lab_weixin.font = FFFontSize(10*ss);
        lab_weixin.textColor = FFColorFromHex(0xc9c9c9);
        lab_weixin.textAlignment = NSTextAlignmentCenter;
        lab_weixin.text = @"微信登录";
        [self addSubview:lab_weixin];
        
        imgv_qq = [[UIImageView alloc] initWithFrame:CGRectZero];
        imgv_qq.userInteractionEnabled = YES;
        imgv_qq.image = FFImg(@"qqLogin");
        [imgv_qq addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ff_tapQQ)]];
        [self addSubview:imgv_qq];
        
        lab_qq = [[UILabel alloc] initWithFrame:CGRectZero];
        lab_qq.font = FFFontSize(10*ss);
        lab_qq.textColor = FFColorFromHex(0xc9c9c9);
        lab_qq.textAlignment = NSTextAlignmentCenter;
        lab_qq.text = @"QQ登录";
        [self addSubview:lab_qq];
        
        [self ff_buildLayout];
    }
    
    return self;
}

#pragma mark - Layout Method

- (void)ff_buildLayout
{
    [lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.width.mas_equalTo(100*ss);
    }];
    
    [v_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(lab.mas_centerY);
        make.left.mas_equalTo(self.mas_left);
        make.size.mas_equalTo(CGSizeMake(SCREENW, 0.3));
    }];
    
    [imgv_weixin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.mas_centerY).offset(-10*ss);;
        make.right.mas_equalTo(self.mas_centerX).offset(-marginForIcon);
        make.size.mas_equalTo(IconWeixinSize);
    }];
    
    [lab_weixin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(imgv_weixin.mas_bottom).offset(marginForLab);
        make.centerX.mas_equalTo(imgv_weixin.mas_centerX);
    }];
    
    [imgv_qq mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(imgv_weixin.mas_centerY);
        make.left.mas_equalTo(self.mas_centerX).offset(marginForIcon);
        make.size.mas_equalTo(IconQQSize);
    }];
    
    [lab_qq mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(imgv_qq.mas_bottom).offset(marginForLab);
        make.centerX.mas_equalTo(imgv_qq.mas_centerX);
    }];
}

#pragma mark - Tap Action Method

- (void)ff_tapWeixin
{
    [self.delegate ff_weixinLogin];
}

- (void)ff_tapQQ
{
    [self.delegate ff_qqLogin];
}
@end
