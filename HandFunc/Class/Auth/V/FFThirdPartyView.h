//
//  FFThirdPartyView.h
//  HandFunc
//
//  Created by ff'Mac on 2017/6/28.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FFThirdPartyViewDelegate;

@interface FFThirdPartyView : UIView

@property (nonatomic, assign) id<FFThirdPartyViewDelegate>delegate;

@end

@protocol FFThirdPartyViewDelegate <NSObject>

- (void)ff_weixinLogin;
- (void)ff_qqLogin;

@end
