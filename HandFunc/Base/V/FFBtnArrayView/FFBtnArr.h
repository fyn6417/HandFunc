//
//  FFBtnArr.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/2.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FFBtnArrDelegate;

@interface FFBtnArr : UIView

@property (nonatomic, assign) id<FFBtnArrDelegate>delegate;

- (instancetype)initWithBtns:(NSArray *)btnArrs imgs:(NSArray *)imgNameArr w:(CGFloat)width;
- (instancetype)initWithBtns:(NSArray *)btnArrs w:(CGFloat)width;
- (instancetype)initWithRoundBtns:(NSArray *)btnArrs w:(CGFloat)width;

@end


@protocol FFBtnArrDelegate <NSObject>

@optional
- (void)ff_tapWithIndex:(NSInteger)index;
- (void)ff_roundTapWithIndex:(NSInteger)index;

@end
