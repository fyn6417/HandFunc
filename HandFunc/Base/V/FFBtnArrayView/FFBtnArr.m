//
//  FFBtnArr.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/2.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "FFBtnArr.h"



@interface FFBtnArr()
{
    
}

@end

@implementation FFBtnArr
/*
- (instancetype)initWithBtns:(NSArray *)btnArrs imgs:(NSArray *)imgNameArr
{
    self = [super init];
    if (self) {
        if (btnArrs != nil && btnArrs.count > 0 && imgNameArr != nil && imgNameArr.count > 0) {
            for (NSInteger i = 0; i < btnArrs.count; i++) {
                CGFloat w = SCREENW/btnArrs.count;
                
                UIView *temp = [[UIView alloc] init];
                temp.frame = CGRectMake(i*w, 0, w, 80*ss);
                temp.backgroundColor = FFWhiteColor;
                temp.layer.borderWidth = 0.5;
                temp.layer.borderColor = [UIColor lightGrayColor].CGColor;
                temp.userInteractionEnabled = YES;
                temp.tag = i;
                UIGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ff_btnTaped:)];
                [temp addGestureRecognizer:tap];
                [self addSubview:temp];
                
                UIImageView *img = [[UIImageView alloc] init];
                img.image = FFImg(imgNameArr[i]);
                img.frame = CGRectMake(0, 0, 50*ss, 50*ss);
                [temp addSubview:img];
                
                [img mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.mas_equalTo(temp.mas_centerX);
                    make.top.mas_equalTo(temp.mas_top).offset(10*ss);
                }];
                
                UILabel *lab = [[UILabel alloc] init];
                lab.text = btnArrs[i];
                lab.textColor = FFBaseColor;
                lab.textAlignment = NSTextAlignmentCenter;
                lab.frame = CGRectMake(0, 0, 100*ss, 30*ss);
                lab.font = FFFontSize(12*ss);
                [temp addSubview:lab];
                
                [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.mas_equalTo(temp.mas_centerX);
                    make.top.mas_equalTo(img.mas_bottom).offset(10*ss);
                }];
            }
        }
    }
    return self;
}*/

- (instancetype)initWithBtns:(NSArray *)btnArrs imgs:(NSArray *)imgNameArr w:(CGFloat)width
{
    self = [super init];
    if (self) {
        self.userInteractionEnabled = YES;
        if (btnArrs != nil && btnArrs.count > 0 && imgNameArr != nil && imgNameArr.count > 0) {
            CGFloat w = width/btnArrs.count;
            for (NSInteger i = 0; i < btnArrs.count; i++) {
                
                UIButton *btn = [[UIButton alloc] verticalImage:FFImg(imgNameArr[i]) Title:btnArrs[i] space:10 titleFont:FFFontSize(12*ss) textAli:NSTextAlignmentLeft normalBgColor:FFBaseColor higtlightBgColor:FFBaseColor target:self sel:@selector(ff_btnTaped:) frame:CGRectMake(i*w, 0, w, 70*ss)];
                btn.titleLabel.textAlignment = NSTextAlignmentCenter;
                btn.frame = CGRectMake(i*w, 0, w, 70*ss);
                btn.tag = i;
                btn.layer.borderWidth = 0.3;
                btn.layer.borderColor = [UIColor lightGrayColor].CGColor;
                [self addSubview:btn];
                
            }
            
        }
    }
    return self;
}

- (instancetype)initWithBtns:(NSArray *)btnArrs w:(CGFloat)width
{
    self = [super init];
    if (self) {
        self.userInteractionEnabled = YES;
        if (btnArrs != nil && btnArrs.count > 0) {
            CGFloat w = width/btnArrs.count;
            for (NSInteger i = 0; i < btnArrs.count; i++) {
                
                FFBtn *btn = [[FFBtn alloc] initWithTitle:btnArrs[i] titleColor:FFBaseBGColor titleFont:FFFontSize(12) bgColor:FFColorFromHex(0xaaaaaa) target:self action:@selector(ff_btnTaped:)];
                
                btn.frame = CGRectMake(i*w, 0, w, 70*ss);
                btn.tag = i;
                btn.layer.borderWidth = 0.3;
                btn.layer.borderColor = [UIColor lightGrayColor].CGColor;
                [self addSubview:btn];
                
            }
            
        }
    }
    return self;
}

- (instancetype)initWithRoundBtns:(NSArray *)btnArrs w:(CGFloat)width
{
    self = [super init];
    if (self) {
        self.userInteractionEnabled = YES;
        if (btnArrs != nil && btnArrs.count > 0) {
            CGFloat w = width/btnArrs.count;
            for (NSInteger i = 0; i < btnArrs.count; i++) {
                
                FFBtn *btn = [[FFBtn alloc] initWithTitle:btnArrs[i] titleColor:FFBaseBGColor titleFont:FFFontSize(12) bgColor:FFColorFromHex(0xaaaaaa) target:self action:@selector(ff_roundBtnTaped:)];
                
                btn.frame = CGRectMake(i*w, 0, w, 30*ss);
                btn.tag = i;
                btn.layer.cornerRadius = 15;
                btn.layer.borderWidth = 0.3;
                btn.layer.borderColor = [UIColor lightGrayColor].CGColor;
                [self addSubview:btn];
                
            }
            
        }
    }
    return self;
}


#pragma mark - Tap Action Method

- (void)ff_btnTaped:(id)sender
{
    UIView *imgBtn = sender;
    NSLog(@"TapTag");
//    NSLog(@"TapTag:%d", imgBtn.tag);
    [self.delegate ff_tapWithIndex:imgBtn.tag];
}

- (void)ff_roundBtnTaped:(id)sender
{
    UIView *imgBtn = sender;
    [self.delegate ff_roundTapWithIndex:imgBtn.tag];
}
@end
