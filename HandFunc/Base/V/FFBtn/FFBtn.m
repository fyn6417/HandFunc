//
//  FFBtn.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/9.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "FFBtn.h"

@implementation FFBtn

- (instancetype)initWithTitle:(NSString *)title
                   titleColor:(UIColor *)tColor
                    titleFont:(UIFont *)font
                      bgColor:(UIColor *)bgColor
                       target:(id)target
                       action:(SEL)action
{
    self = [super init];
    if (self) {
        self = [FFBtn buttonWithType:UIButtonTypeCustom];
        self.backgroundColor = bgColor;
        self.frame = CGRectZero;
        [self setTitle:title forState:UIControlStateNormal];
        self.titleLabel.textColor = tColor;
        self.titleLabel.font = font;
        
        [self addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (instancetype)initWithImg:(UIImage *)img
                      Title:(NSString *)title
                 titleColor:(UIColor *)tColor
                  titleFont:(UIFont *)font
                    bgColor:(UIColor *)bgColor
                     target:(id)target
                     action:(SEL)action
{
    self = [super init];
    if (self) {
        self = [self initWithTitle:title titleColor:tColor titleFont:font bgColor:bgColor target:target action:action];
        [self setImage:img forState:UIControlStateNormal];
    }
    return self;
}


@end
