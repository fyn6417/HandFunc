//
//  FFBtn.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/9.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FFBtn : UIButton

- (instancetype)initWithTitle:(NSString *)title
                   titleColor:(UIColor *)tColor
                    titleFont:(UIFont *)font
                      bgColor:(UIColor *)bgColor
                       target:(id)target
                       action:(SEL)action;

@end
