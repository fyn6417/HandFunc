//
//  FFTextField.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/5.
//  Copyright © 2017年 ff. All rights reserved.
//  登录用

#import "FFLoginTextField.h"



@interface FFLoginTextField()<UITextFieldDelegate>
{
    UIImageView *imgv_icon;
    UIView *v_line;
//    UITextField *tf_content;
}

@end

@implementation FFLoginTextField

- (instancetype)initWithIcon:(UIImage *)img placeHolder:(NSString *)placeHolder
{
    self = [super init];
    if (self) {
        
        self.userInteractionEnabled = YES;
        
        imgv_icon = [[UIImageView alloc] initWithFrame:CGRectZero];
        imgv_icon.image = img;
        [self addSubview:imgv_icon];
        
        v_line = [[UIView alloc] initWithFrame:CGRectZero];
        v_line.backgroundColor = FFColorFromHex(0x60c1bc);
        [self addSubview:v_line];
        
        _tf_content = [[UITextField alloc] initWithFrame:CGRectZero];
        _tf_content.userInteractionEnabled = YES;
        _tf_content.attributedPlaceholder = [NSMutableString ff_attributedStringWithString:placeHolder color:FFColorFromHex(0x60c1bc) font:FFFontSize(12*ss)];
        _tf_content.textAlignment = NSTextAlignmentLeft;
        _tf_content.textColor = FFColorFromHex(0x60c1bc);
        _tf_content.font = FFFontSize(16*ss);
        [self addSubview:_tf_content];
        
        [self ff_layoutViews];
    }
    return self;
}

- (void)ff_layoutViews
{
    [imgv_icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.mas_centerY);
        make.left.mas_equalTo(self.mas_left);
        make.size.mas_equalTo(CGSizeMake(20*ss, 25*ss));
    }];
    
    [v_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left);
        make.bottom.mas_equalTo(self.mas_bottom);
        make.right.mas_equalTo(self.mas_right);
        make.height.mas_equalTo(0.3f);
    }];
    
    [_tf_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imgv_icon.mas_right).offset(15*ss);
        make.centerY.mas_equalTo(imgv_icon.mas_centerY);
        make.size.mas_equalTo(CGSizeMake((282-25-21)*ss, 25*ss));
    }];
}


@end
