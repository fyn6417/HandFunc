//
//  FFLoginTextField.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/5.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FFLoginTextFieldDelegate;

@interface FFLoginTextField : UIView
@property (nonatomic, strong) UITextField *tf_content;
@property (nonatomic, assign) id<FFLoginTextFieldDelegate>delegate;

- (instancetype)initWithIcon:(UIImage *)imgName placeHolder:(NSString *)placeHolder;

@end

@protocol FFLoginTextFieldDelegate <NSObject>

@end
