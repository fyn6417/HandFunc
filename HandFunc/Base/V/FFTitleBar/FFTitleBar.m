//
//  FFTitleBar.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/8.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "FFTitleBar.h"

@interface FFTitleBar()
{
    UILabel *lab_title;
    UIButton *btn_left;
    UIButton *btn_right;
}

@end

@implementation FFTitleBar

- (instancetype)initWithTitle:(NSString *)title leftImg:(UIImage *)lImg rightImg:(UIImage *)rImg
{
    self = [super init];
    if (self) {
        [self ff_title:title leftBtnImg:lImg];
        [self ff_rightBtnWithImg:rImg];
        [self ff_layoutView];
    }
    return self;
}

- (instancetype)initWithTitle:(NSString *)title leftImg:(UIImage *)lImg rightName:(NSString *)str
{
    self = [super init];
    if (self) {
        [self ff_title:title leftBtnImg:lImg];
        [self ff_rightBtnWithName:str];
        [self ff_layoutView];
    }
    return self;
}


#pragma mark - Init Method

- (void)ff_title:(NSString *)str leftBtnImg:(UIImage *)img
{
    self.backgroundColor = FFBaseBGColor;
    
    lab_title = [[UILabel alloc] initWithFrame:CGRectZero];
    lab_title.text = str;
//    lab_title.textColor = FFColorFromHex(0x414141);
    lab_title.textColor = FFBlackColor;
    lab_title.textAlignment = NSTextAlignmentCenter;
    lab_title.font = [UIFont boldSystemFontOfSize:18*ss];
    [self addSubview:lab_title];
    
    btn_left = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_left setImage:img forState:UIControlStateNormal];
    btn_left.frame = CGRectZero;
    [btn_left addTarget:self action:@selector(btn_action:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn_left];
}


- (void)ff_rightBtnWithImg:(UIImage *)img
{
    btn_right = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_right setImage:img forState:UIControlStateNormal];
    btn_right.frame = CGRectZero;
    [btn_right addTarget:self action:@selector(btn_action:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn_right];
}

- (void)ff_rightBtnWithName:(NSString *)str
{
    btn_right = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_right setTitle:str forState:UIControlStateNormal];
    [btn_right setTitleColor:FFBaseColor forState:UIControlStateNormal];
    btn_right.titleLabel.font = FFFontSize(12*ss);
    btn_right.frame = CGRectZero;
    [btn_right addTarget:self action:@selector(btn_action:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn_right];
}

#pragma mark - Layout Method

- (void)ff_layoutView
{
    [lab_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.centerY.mas_equalTo(self.mas_centerY).offset(9*ss);
    }];
    
    [btn_left mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(18*ss);
        make.centerY.mas_equalTo(lab_title.mas_centerY);
    }];
    
    [btn_right mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_left).offset(-18*ss);
        make.centerY.mas_equalTo(lab_title.mas_centerY);
    }];
}

#pragma mark - Btn Action Method

- (void)btn_action:(UIButton *)sender
{
    if (sender == btn_left) {
        [self.delegate ff_leftBtnClicked];
    } else {
        [self.delegate ff_rightBtnClicked];
    }
}

@end
