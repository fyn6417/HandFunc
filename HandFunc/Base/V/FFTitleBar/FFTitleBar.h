//
//  FFTitleBar.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/8.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FFTitleBarDelegate;

@interface FFTitleBar : UIView

@property (nonatomic, assign) id<FFTitleBarDelegate>delegate;

- (instancetype)initWithTitle:(NSString *)title leftImg:(UIImage *)img rightImg:(UIImage *)img;
- (instancetype)initWithTitle:(NSString *)title leftImg:(UIImage *)lImg rightName:(NSString *)str;

@end

@protocol FFTitleBarDelegate <NSObject>

@optional

- (void)ff_leftBtnClicked;
- (void)ff_rightBtnClicked;

@end
