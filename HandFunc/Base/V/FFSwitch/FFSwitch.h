//
//  FFSwitch.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/15.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FFSwitch : UISwitch

- (instancetype)initWithFrame:(CGRect)frame OnColor:(UIColor *)color on:(BOOL)on target:(id)target sel:(SEL)sel;

@end
