//
//  FFSwitch.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/15.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "FFSwitch.h"

@implementation FFSwitch

- (instancetype)initWithFrame:(CGRect)frame
                      OnColor:(UIColor *)color
                           on:(BOOL)on
                       target:(id)target
                          sel:(SEL)sel
{
    self = [super init];
    if (self) {
        self =  [[FFSwitch alloc] initWithFrame:frame];
        self.onTintColor = color;
        self.on = on;
        [self addTarget:target action:sel forControlEvents:UIControlEventValueChanged];
    }
    return self;
}

@end
