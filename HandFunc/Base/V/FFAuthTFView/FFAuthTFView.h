//
//  FFAuthTFView.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/6.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FFAuthTFView : UIView

@property (nonatomic, strong) UITextField *tf_content;

- (instancetype)initWithPlaceHolder:(NSString *)placeHolder;
- (instancetype)initWithPlaceHolder:(NSString *)placeHolder labText:(NSString *)text;

@end
