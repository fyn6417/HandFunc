//
//  FFAuthTFView.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/6.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "FFAuthTFView.h"

@interface FFAuthTFView()
{
    UIView *v_line;
    UILabel *lab_code;
    NSInteger sec;
    NSTimer *countTimer;
}

@end

@implementation FFAuthTFView

- (instancetype)initWithPlaceHolder:(NSString *)placeHolder labText:(NSString *)text
{
    self = [super init];
    if (self) {
        sec = 60;
        v_line = [[UIView alloc] initWithFrame:CGRectZero];
        v_line.backgroundColor = FFBaseColor;
        [self addSubview:v_line];
        
        _tf_content = [[UITextField alloc] initWithFrame:CGRectZero];
        _tf_content.userInteractionEnabled = YES;
        _tf_content.attributedPlaceholder = [NSMutableString ff_attributedStringWithString:placeHolder color:FFColorFromHex(0xaaaaaa) font:FFFontSize(12*ss)];
        _tf_content.textAlignment = NSTextAlignmentLeft;
        _tf_content.textColor = FFColorFromHex(0x414141);
        _tf_content.font = FFFontSize(12*ss);
        [self addSubview:_tf_content];
        
        lab_code = [[UILabel alloc] init];
        lab_code.text = text;
        lab_code.font = FFFontSize(15*ss);
        lab_code.textColor = FFColorFromHex(0x414141);
        lab_code.textAlignment = NSTextAlignmentCenter;
        lab_code.userInteractionEnabled = YES;
        [lab_code addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ff_sendCode)]];
        [self addSubview:lab_code];
        
        [self ff_layoutViewsWithBtn];
    }
    return self;
}

- (instancetype)initWithPlaceHolder:(NSString *)placeHolder
{
    self = [super init];
    if (self) {
        sec = 60;
        
        v_line = [[UIView alloc] initWithFrame:CGRectZero];
        v_line.backgroundColor = FFBaseColor;
        [self addSubview:v_line];
        
        _tf_content = [[UITextField alloc] initWithFrame:CGRectZero];
        _tf_content.userInteractionEnabled = YES;
        _tf_content.attributedPlaceholder = [NSMutableString ff_attributedStringWithString:placeHolder color:FFColorFromHex(0xaaaaaa) font:FFFontSize(12*ss)];
        _tf_content.textAlignment = NSTextAlignmentLeft;
        _tf_content.textColor = FFColorFromHex(0x414141);
        _tf_content.font = FFFontSize(12*ss);
        [self addSubview:_tf_content];
        
        [self ff_layoutViews];
    }
    return self;
}

- (void)ff_layoutViews
{
    [v_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.mas_bottom);
        make.left.mas_equalTo(self.mas_left);
        make.right.mas_equalTo(self.mas_right);
        make.height.mas_equalTo(0.3f);
    }];
    
    [_tf_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(v_line.mas_centerX);
        make.bottom.mas_equalTo(v_line.mas_top).offset(-7*ss);
        make.size.mas_equalTo(CGSizeMake(282*ss, 25*ss));
    }];
}

- (void)ff_layoutViewsWithBtn
{
    [v_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.mas_bottom);
        make.left.mas_equalTo(self.mas_left);
        make.right.mas_equalTo(self.mas_right);
        make.height.mas_equalTo(0.3f);
    }];
    
    [_tf_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(v_line.mas_left);
        make.bottom.mas_equalTo(v_line.mas_top).offset(-7*ss);
//        make.right.mas_equalTo(lab_code.mas_left);
        make.size.mas_equalTo(CGSizeMake(180*ss, 25*ss));
    }];
    
    [lab_code mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(v_line.mas_right);
        make.centerY.mas_equalTo(_tf_content.mas_centerY);
    }];
}

#pragma mark - Tap Action Method

- (void)ff_sendCode
{
    NSLog(@"💯获取验证码");
    lab_code.textColor = FFColorFromHex(0xaaaaaa);
    lab_code.text = [NSString stringWithFormat:@"%lds", (long)sec];
    lab_code.userInteractionEnabled = NO;
    countTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(ff_changeSec) userInfo:nil repeats:YES];
}

#pragma mark - Timer Action Method

- (void)ff_changeSec
{
    sec--;
    lab_code.text = [NSString stringWithFormat:@"%lds", (long)sec];
    if (sec == 0) {
        NSLog(@"💯重新获取验证码");
        [countTimer invalidate];
        lab_code.userInteractionEnabled = YES;
        lab_code.textColor = FFColorFromHex(0x414141);
        lab_code.text = @"重新获取验证码";
        sec = 60;
    }
    
    NSLog(@"💯还有%ld", (long)sec);
}


@end
