//
//  FFLab.h
//  HandFunc
//
//  Created by ff'Mac on 2017/7/11.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FFLab : UILabel

- (instancetype)initWithText:(NSString *)text txtColor:(UIColor *)color txtAli:(NSTextAlignment)ali font:(UIFont *)font;

@end
