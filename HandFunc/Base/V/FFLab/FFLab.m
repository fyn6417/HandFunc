//
//  FFLab.m
//  HandFunc
//
//  Created by ff'Mac on 2017/7/11.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "FFLab.h"

@implementation FFLab

- (instancetype)initWithText:(NSString *)text txtColor:(UIColor *)color txtAli:(NSTextAlignment)ali font:(UIFont *)font
{
    self = [super init];
    if (self) {
        self = [[FFLab alloc] initWithFrame:CGRectZero];
        self.text = text;
        self.numberOfLines = 0;
        self.textColor = color;
        self.textAlignment = ali;
        self.font = font;
    }
    return self;
}

@end
