//
//  FFURL.h
//  HandFunc
//
//  Created by ff'Mac on 2017/6/28.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const FF;

extern NSString * const kJPush;

@interface FFURL : NSObject

extern NSString *const fKeyUserName;
extern NSString *const fKeyUserPWD;
extern NSString *const fKeyLogin;
extern NSString *const fKeyWeixin;
extern NSString *const fKeyBackBtnImg;

@end
