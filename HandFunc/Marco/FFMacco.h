//
//  FFMacco.h
//  HandFunc
//
//  Created by ff'Mac on 2017/6/28.
//  Copyright © 2017年 ff. All rights reserved.
//

#ifndef FFMacco_h
#define FFMacco_h



//Img
#define FFImg(imageName) [UIImage imageNamed:[NSString stringWithFormat:@"%@",imageName]]
#define FFViewBorderRadius(View, Radius, BorderWidth, BorderColor)\
[View.layer setCornerRadius:(Radius/2)];\
[View.layer setMasksToBounds:YES];\
[View.layer setBorderWidth:(BorderWidth)];\
[View.layer setBorderColor:[BorderColor CGColor]] // view圆角


//File
#define PathDocument [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]


//Color
#define FFClearColor [UIColor clearColor]
#define FFGreenColor [UIColor greenColor]
#define FFWhiteColor [UIColor whiteColor]
#define FFBlackColor [UIColor blackColor]
#define FFGrayColor  [UIColor grayColor]
#define FFRedColor   [UIColor redColor]
#define FFColorFromHex(s)  [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s & 0xFF00) >> 8))/255.0 blue:((s & 0xFF))/255.0  alpha:1.0]

#define FFBaseColor    FFColorFromHex(0x09aea5)
#define FFBaseBGColor  FFColorFromHex(0xf2f2f2)

//Notification
#define FF_NOTIF_ADD(n, method)     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(method) name:n object:nil]
#define FF_NOTIF_POST(n, o)    [[NSNotificationCenter defaultCenter] postNotificationName:n object:o]
#define FF_NOTIF_REMV()        [[NSNotificationCenter defaultCenter] removeObserver:self]

//Rect
#define SCREENW [UIScreen mainScreen].bounds.size.width
#define SCREENH [UIScreen mainScreen].bounds.size.height
#define ss [FFUtils ff_ss]
#define fKeyRowH 44*ss

//Font
#define FFFontSize(v) [UIFont systemFontOfSize:v]
#define FFBoldFontSize(v) [UIFont boldSystemFontOfSize:v]

//Layer
#define FFCorner   5


#endif /* FFMacco_h */
