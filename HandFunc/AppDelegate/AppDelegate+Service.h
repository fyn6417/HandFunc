//
//  AppDelegate+Service.h
//  HandFunc
//
//  Created by ff'Mac on 2017/6/28.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "AppDelegate.h"
#import "MainTabBarVC.h"
#import "LoginVC.h"
#import <WXApi.h>

@interface AppDelegate (Service)

- (void)ff_initRootWindows;

- (void)ff_weixinRegister;

@end
