//
//  AppDelegate+Service.m
//  HandFunc
//
//  Created by ff'Mac on 2017/6/28.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "AppDelegate+Service.h"

@implementation AppDelegate (Service)

#pragma mark - RootVC Method

- (void)ff_initRootWindows
{
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = [MainTabBarVC new];
    self.window.backgroundColor = FFClearColor;
    [self.window makeKeyAndVisible];
}

#pragma mark - WX Method

- (void)ff_weixinRegister
{
    [WXApi registerApp:fKeyWeixin];
}

#pragma mark - QQ Method


#pragma mark - Push Method


@end
