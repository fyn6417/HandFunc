//
//  FFUtils.h
//  HandFunc
//
//  Created by ff'Mac on 2017/6/29.
//  Copyright © 2017年 ff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FFUtils : NSObject

/**
 是否登录

 @return Yes 登录
 */
+ (BOOL)ff_isLogin;

/**
 缩放因子

 @return 4.7 1
 */
+ (CGFloat)ff_ss;

/**
 系统版本号

 @return 10.1
 */
+ (NSString *)ff_systemVerson;
+ (NSString *)ff_appVerson;
+ (NSString *)ff_appName;
@end
