//
//  FFUtils.m
//  HandFunc
//
//  Created by ff'Mac on 2017/6/29.
//  Copyright © 2017年 ff. All rights reserved.
//

#import "FFUtils.h"

@implementation FFUtils

+ (BOOL)ff_isLogin
{
    NSString *k = [[NSUserDefaults standardUserDefaults] objectForKey:fKeyLogin];
    if (k != nil && k.length != 0) {
        return YES;
    } else {
        return NO;
    }
}

+ (CGFloat)ff_ss
{
    CGFloat sss;
    if (SCREENH == 1136)
    {
        sss = 640/750;
    } else if (SCREENH == 1334)
    {
        sss = 1;
    } else
    {
        sss = 1080/750;
    }
    return sss;
}

+ (NSArray *)ff_appName
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    return [infoDictionary objectForKey:@"CFBundleDisplayName"];
}

+ (NSString *)ff_appVerson
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    return [infoDictionary objectForKey:@"CFBundleShortVersionString"];
}

+ (NSString *)ff_systemVerson
{
    return [[UIDevice currentDevice] systemVersion];
}

@end
